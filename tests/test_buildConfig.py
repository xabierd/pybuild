from tests import *

import json
import getpass



class TestBuildConfig(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.sample_bamboo_build = {
            'bamboo_agentId': '42696748',
            'bamboo_agentWorkingDirectory': '/opt/ds/bamboo-2/xml-data/build-dir',
            'bamboo_artifactory_path': 'ML/test',
            'bamboo_artifactory_repo': 'sandbox',
            'bamboo_build_working_directory': '/opt/ds/bamboo-2/xml-data/build-dir/ML-MLT3-PC',
            'bamboo_buildKey': 'ML-MLT3-PC',
            'bamboo_buildNumber': '328',
            'bamboo_buildPlanName': 'Market Links - ML_Template_v3_DO_NOT_USE - Pre-Build Checks',
            'bamboo_buildResultKey': 'ML-MLT3-PC-328',
            'bamboo_buildResultsUrl': 'http://bamboo.ams.optiver.com/browse/ML-MLT3-PC-328',
            'bamboo_buildTimeStamp': '2014-11-07T22:00:22.642+01:00',
            'bamboo_inject_somevar': 'somevalue',
            'bamboo_jira_username': 'notjira',
            'bamboo_jira_version': 'notjira',
            'bamboo_inject_version': '9.9.9',
            'bamboo_loglevel': 'INFO',
            'bamboo_planKey': 'ML-MLT3',
            'bamboo_planName': 'Market Links - ML_Template_v3_DO_NOT_USE',
            'bamboo_planRepository_branchName': 'trunk',
            'bamboo_planRepository_name': 'ML Project',
            'bamboo_planRepository_previousRevision': '367',
            'bamboo_planRepository_repositoryUrl': 'http://svn.ams.optiver.com/svn/test_ams/ML/gw_ing_fix_e-TEST/trunk',
            'bamboo_planRepository_revision': '367',
            'bamboo_planRepository_type': 'optiver-svn',
            'bamboo_product_name': 'ml_template',
            'bamboo_release_notes_extra_args': '',
            'bamboo_repository_branch_name': 'trunk',
            'bamboo_repository_git_branch': 'ML_Build_v3',
            'bamboo_repository_git_repositoryUrl': 'ssh://git@stash.ams.optiver.com:7999/ml/ml-build-scripts.git',
            'bamboo_repository_git_username': '',
            'bamboo_repository_name': 'DS - pyBuild - DEV',
            'bamboo_repository_optiversvn_repositoryUrl': 'http://svn.ams.optiver.com/svn/test_ams/ML/gw_ing_fix_e-TEST/trunk',
            'bamboo_repository_previous_revision_number': '367',
            'bamboo_repository_revision_number': '367',
            'bamboo_resultsUrl': 'http://bamboo.ams.optiver.com/browse/ML-MLT3-PC-328',
            'bamboo_shortJobKey': 'PC',
            'bamboo_shortJobName': 'Pre-Build Checks',
            'bamboo_shortPlanKey': 'MLT3',
            'bamboo_shortPlanName': 'ML_Template_v3_DO_NOT_USE',
            'bamboo_working_directory': '/opt/ds/bamboo-2/xml-data/build-dir/ML-MLT3-PC',

        }
        cls.sample_config = {
            'productName': 'some_product',
            'productVersion': 'some_product-1.2.3_alpha',
            'buildNumber': 123,
            'planName': 'Manual random build',
            'artifactoryRepo': 'sabdbox',
            'artifactoryPath': 'DS/test',
            'vcsURL': 'http://svn.ams.optiver.com/svn/test_ams/ML/gw_ing_fix_e-TEST/branches/pof',
            'vcsRevision': 341,
            'vcsRepoType': 'svn',
            'vcsRepoBranch': 'pof',
        }
        cls.bamboo = BambooClient()

    def set_environment(self, env):
        for k in env:
            try:
                os.environ[k] = env[k]
            except Exception, e: # pragma: no cover
                print e.message

    @mock.patch('pybuild.managers.BambooClient.get_server_version')
    def test_init_bamboo_build(self, mock_get_bamboo_version):

        self.set_environment(self.sample_bamboo_build)
        mock_get_bamboo_version.return_value = "Bamboo 9.9.99"

        environments = {
            'jira_version':
                {'bamboo_jira_version': 'beer-1.2.3',
                 'expected': '1.2.3'},
            'inject_version':
                {'bamboo_jira_version': '',
                 'bamboo_inject_version': '4.5.6',
                 'expected': '4.5.6'}
        }

        for e in environments:
            self.set_environment(environments[e])
            build_config = BuildConfig(bamboo=self.bamboo)
            self.assertEquals(environments[e]['expected'], build_config.get_build_version(),
                              msg='Expected version: %s, buildConfig version: %s' % (
                                  environments[e]['expected'],
                                  build_config.get_build_version()
                              ))

"""
    @mock.patch('pybuild.managers.BambooClient.get_build_report')
    @mock.patch('pybuild.managers.BambooClient.get_server_version')
    def test_get_build_user(self, mock_get_bamboo_version, mock_get_build_report):
        self.set_environment(self.sample_bamboo_build)
        mock_get_bamboo_version.return_value = "Bamboo 9.9.99"

        plans = {
            'manual':
                {'bamboo_buildKey': 'ML-MLT3-PRE',
                 'bamboo_buildNumber': '564',
                 'bamboo_jira_username': '',
                 'expected': getpass.getuser()},}
            'scheduled':
                {'bamboo_buildKey': 'ML-MLT3-PRE',
                 'bamboo_buildNumber': '565',
                 'bamboo_jira_username': '',
                 'expected': getuser()},}
        'jira1':
                {'bamboo_buildKey': 'ML-MLT3-PRE',
                 'bamboo_buildNumber': '327',
                 'bamboo_jira_username': 'some_jira_user',
                 'expected': 'some_jira_user'},
            'jira2':
                {'bamboo_buildKey': 'ML-MLT3-PRE',
                 'bamboo_buildNumber': '330',
                 'bamboo_jira_username': '',
                 'expected': 'xabierdavila'}
        }
        for p in plans:
            self.set_environment(plans[p])
            build_config = BuildConfig(bamboo=self.bamboo)
            mock_get_build_report.return_value = json.loads('{"reasonSummary": "Manual run by <a href=http://bamboo.ams.optiver.com/browse/user/XabierDavila>Xabier Davila</a>"}')
            buildUser = build_config.get_build_user()
            self.assertEquals(plans[p]['expected'], buildUser,
                              msg='%s: Expected user: %s, buildConfig user: %s' % (
                                  p, plans[p]['expected'],
                                  buildUser))
"""