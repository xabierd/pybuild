Bamboo
======

.. toctree::
   :maxdepth: 2

.. automodule:: pybuild.managers
.. autoclass:: BambooClient
    :members: