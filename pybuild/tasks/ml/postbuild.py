"""ML post-build checks
"""

from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)

from pybuild.conf import settings
from pybuild.tasks.postbuild import PostBuild

import nose
import sys

class PostbuildML(PostBuild):
    """Marketlinks post-build checks
    """

    def __init__(self):
        PostBuild.__init__(self)

    @staticmethod
    def configure_parser(parser):
        """ Configure parser for this action
        """
        PostBuild.configure_parser(parser)

    def configure(self):
        PostBuild.configure(self)

    def do(self, parser):
        #GenericPostBuild.do(self) No need to run this
        checks = 'pybuild.checks.ml:PostBuildChecks'

        result = nose.run(argv=settings.DEFAULT_NOSE_ARGUMENTS, defaultTest=checks )
        if result:
            log.info(log_header('.') % 'Post-build checks succeed')
        else:
            log.error(log_header('!') % 'Post-build checks failed')
            sys.exit(-1)


