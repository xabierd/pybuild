from ..common import initialize_logger, RestAPIClient
log = initialize_logger(__name__)

from ..conf import settings

import simplejson as json


class StashClient(RestAPIClient):

    _core_api_name = '/rest/api'
    _core_api_version = '1.0'
    _core_api_path = '{0}/{1}'.format(_core_api_name, _core_api_version)
    _core_repo = '{core}/projects/{project}/repos/{repo}'
    _core_tags = '{core_repo}/tags'.format(core_repo=_core_repo)
    _core_branches = '{core_repo}/branches'.format(core_repo=_core_repo)

    _git_api_name = '/rest/git'
    _git_api_version = '1.0'
    _git_api_path = '{0}/{1}'.format(_git_api_name, _git_api_version)
    _git_repo = '{git}/projects/{project}/repos/{repo}'
    _git_tags = '{git_repo}/tags'.format(git_repo=_git_repo)

    def __init__(self, url=settings.STASH_URL, auth=None, usr=None, pwd=None):
        if not auth and not usr:
            auth = self.get_auth('stash')
        RestAPIClient.__init__(self, url, auth=auth, usr=usr, pwd=pwd)

    def get_tags(self, project, repo):
        """
        Get tags from a Stash repo

        :param project: Stash project key
        :param repo: Stash repo slug
        :return: List with tags
        """
        url = self._core_tags.format(core = self._core_api_path, project=project, repo=repo)
        resp = self.rest_get(url)
        # resp.raise_for_status()
        if resp.status_code != 200:
            log.error("Error retrieving tags: {} {}".format(resp.status_code, resp.text))
            return None
        tags = json.loads(resp.text)
        return [t['displayId'] for t in tags['values']]

    def get_branches(self, project, repo):
        """
        Get branches from a Stash repo

        :param project: Stash project key
        :param repo: Stash repo slug
        :return: List with tags
        """
        url = self._core_branches.format(core = self._core_api_path, project=project, repo=repo)
        resp = self.rest_get(url)
        if resp.status_code != 200:
            log.error("Error retrieving tags: {} {}".format(resp.status_code, resp.text))
            return None
        tags = json.loads(resp.text)
        return [t['displayId'] for t in tags['values']]

    def create_tag(self, project, repo, tag_name, message, revision):
        tag_json = {
            "force": "false",
            "message": message,
            "name": tag_name,
            "startPoint": revision,
            "type": "ANNOTATED"
        }
        url = self._git_tags.format(git=self._git_api_path, project=project, repo=repo)
        resp = self.rest_post(url, data=json.dumps(tag_json, sort_keys=True))
        if resp.status_code == 201:
            log.info('Tag {tag} created in {project}/{repo}'.format(tag=tag_name, project=project, repo=repo))
            return resp.text
        log.error('Can\'t create tag {tag}: {msg}'.format(tag=tag_name, msg=resp.text))
        return False