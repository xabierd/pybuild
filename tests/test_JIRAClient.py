from tests import *
from nose.tools import nottest

class TestJIRAClient(TestCase):

    @classmethod
    def setUp(cls):
        cls.patcher = mock.patch('pybuild.common.restAPIClient.RestAPIClient.rest_get', fake_rest_get('jira'))
        cls.patcher.start()
        cls.client = JIRAClient()

    def tearDown(self):
        self.patcher.stop()

    def test_get_project_by_key(self):
        print self.client.get_project_by_key('PRJKEY')

    def test_get_version_details(self):
        print self.client.get_version_details('ml_template-0.4', projectKey='PRJKEY')

    def test_get_versions(self):
        response = self.client.get_versions('PRJKEY')
        #TODO: self.client.rest_get.assert_called_with('sdsd')
        # response2 = self.client.get_versions('TEST')

    @nottest
    def test_get_issues_from_jql(self):
        self.fail()

    def test_version_exists(self):
        self.client = JIRAClient(projectKey='PRJKEY')
        self.assertTrue(self.client.version_exists('ml_template-2.5.1'))
        self.assertFalse(self.client.version_exists('ml_template-9.9'))

    def test_version_released(self):
        self.client = JIRAClient(projectKey='PRJKEY')
        self.assertTrue(self.client.version_released('ml_template-2.5.1'))
        self.assertFalse(self.client.version_released('0.9.2'))
        self.assertFalse(self.client.version_released('9.9.9'))



