from pybuild.common import *
log = initialize_logger(__name__)

from requests import Response
from urllib2 import HTTPError
from unittest import TestCase
import mock

from pybuild.managers import *
from pybuild.build import *
from pybuild.common import *
from optiverversion import LongVersion

from urlparse import urlparse
import os


def fake_rest_get(app):

    def _fake_rest_get(_, url=None, params=None, headers=None):
        """
        A stub urlopen() implementation that load json responses from
        the filesystem.
        """
        # Map path from url to a file
        parsed_url = urlparse(url)

        resource_file = os.path.normpath('tests/resources/%s%s' % (app, parsed_url.path))
        # Must return a file-like object ????
        #with open(resource_file, mode='rb') as response:
        #    return response.read(), 200
        try:
            resp = Response()  # Requests response object
            resp.status_code = 200
            resp._content = open(resource_file, mode='rb').read()
            #return open(resource_file, mode='rb')
            return resp
        except IOError:
            raise HTTPError(url, 404,
                            "HTTP Error 404: Not Found", {}, None)
    return _fake_rest_get

def fake_rest_post(app):

    def _fake_rest_post(_, url=None, params=None, headers=None, data=None):
        """
        A stub urlopen() implementation that load json responses from
        the filesystem.
        """
        # Map path from url to a file
        parsed_url = urlparse(url)

        resource_file = os.path.normpath('tests/resources/%s%s' % (app, parsed_url.path))
        # Must return a file-like object ????
        #with open(resource_file, mode='rb') as response:
        #    return response.read(), 200
        try:
            resp = Response()  # Requests response object
            resp.status_code = 201
            resp._content = open(resource_file, mode='rb').read()
            #return open(resource_file, mode='rb')
            return resp
        except IOError:
            raise HTTPError(url, 404,
                            "HTTP Error 404: Not Found", {}, None)
    return _fake_rest_post