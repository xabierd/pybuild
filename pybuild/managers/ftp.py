from ..common import initialize_logger
log = initialize_logger(__name__)

from ..conf import settings

import ftplib
import os, sys

import logging
log.setLevel(logging.INFO)

class Ftp(ftplib.FTP):
    def __init__(self, server=settings.FTP_SHARE_URL, user=None, passwd=None, root=''):
        self.root = root
        try:
            ftplib.FTP.__init__(self, host=server, user=user, passwd=passwd, timeout=20)
            self.login()
            self.set_debuglevel(0)
        except Exception, e:
            log.error('Error logging in: %s' % e.message)
            sys.exit(-1)
        try:
            self.cwd(root)
        except ftplib.error_perm:
            log.error('Remote root folder "%s" does not exist')
            sys.exit(-1)

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()


    def cwd(self, dirname, create=False):
        """
        Change ftp to dirname. Optioanlly, create the folder in case it does not exist
        """
        log.debug('Current path: %s. Changing to %s' % (self.pwd(), dirname))
        try:
            ftplib.FTP.cwd(self, dirname)
        except ftplib.error_perm:
            if create:
                ftplib.FTP.mkd(self, dirname)
                ftplib.FTP.cwd(self, dirname)

    def mkd(self, dirname, fail_if_exists=False):
        """
        Creates a folder in the current ftp remote path
        :param dirname: folder name
        :param fail_if_exists:
        """
        log.debug('Creating folder: %s' % dirname)
        try:
            dirname = dirname.strip('/')
            if '/' in dirname:
                back = ''
                for dir in dirname.split('/'):
                    self.cwd(dir, create=True)
                    back +='../'
                self.cwd(back)
            else:
                ftplib.FTP.mkd(self, dirname)
        except ftplib.error_perm, e:
            log.debug('Error creating %s: %s' % (dirname, e.message))
            if fail_if_exists:
                raise ftplib.error_perm


    def put(self, local_path, remote_path=None):
        """
        Puts contents of local path into remote path.
        If remote path is empty, content will be uploaded to the current ftp folder
        """
        log.debug('Uploading %s to %s:%s' % (local_path, self.host, remote_path))
        if remote_path:
            log.debug('Current path: %s. Changing to: %s' % (self.pwd(), remote_path))
            self.cwd(remote_path)
        if os.path.isdir(local_path):
            for fname in os.listdir(local_path):
                ffullname = os.path.join(local_path, fname)
                if os.path.isfile(ffullname):
                    # Upload file
                    with open(ffullname, 'rb') as f:
                        self.storbinary('STOR ' + fname, f)
                else:
                    # Create directory
                    log.debug('Creating directory %s ...' % fname)
                    self.mkd(fname)
                    # Go recursively
                    self.cwd(fname)
                    self.put(ffullname)
                    self.cwd('..')
        else:
            # Upload file
            with open(local_path, 'rb') as f:
                self.storbinary('STOR ' + local_path, f)





