from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)

from pybuild.tasks.publish import Publish

class PublishML(Publish):
    """Marketlinks publish to Artifactory
    """

    def __init__(self):
        Publish.__init__(self)

    @staticmethod
    def configure_parser(parser):
        Publish.configure_parser(parser)

    def configure(self):
        Publish.configure(self)

    def do(self, parser):
        Publish.do(self, parser)
