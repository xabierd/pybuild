"""
Generic Pre-Build checks
"""
from ..common import initialize_logger, log_header, run_command, url_exists
log = initialize_logger(__name__)

from pybuild.conf import settings

from pybuild.managers import Artifactory, BambooClient, ConfluenceClient, JIRAClient,Svn, Ftp, LDAPOptiver
from pybuild.build import Build

from unittest import TestCase, SkipTest

import re, os


class PreBuildChecks(TestCase):

    @classmethod
    def setUpClass(cls):
        # Configuration, initialize everything for testing purposes
        cls.build = Build(artifactory=Artifactory(),
                          bamboo=BambooClient(),
                          jira=JIRAClient(),
                          confluence=ConfluenceClient())
        cls.build.init_vcs()
        #log.info('Build: \n%s' % cls.build)

    def test_version_format(self):
        """Check that the version format is correct"""
        self.assertIsNotNone(re.search(settings.VERSION_VALIDATION_REGEX, self.build.full_version_tag),
                             msg='Version %s does not follow the required format')

    def test_tag_does_not_exist(self):
        """Check that the tag we're building doesn't already exist"""
        self.assertFalse(self.build.vcs.tag_exists(self.build.full_version_tag),
                         msg='The tag {tag} already exists in {repo}'.format(
                             tag=self.build.full_version_tag,
                             repo=self.build.vcs.url
                         ))

    def test_binary_version_has_not_been_released(self):
        """Check that the binary version we're building has not already been released"""
        # Checks for path in releases: optiver-releases/CS/fabric/0.6
        url = '/'.join([self.build.artifactoryReleasesRepo,
                        self.build.artifactoryPath,
                        self.build.productName,
                        self.build.get_build_version()]
        )
        self.assertFalse(url_exists('/'.join([self.build.artifactory.server_url, url])),
                         msg='The binary version you\'re building ({version}) has already been released to Artifactory: {path}'.format(
                             version=self.build.get_build_version(),
                             path=url)
                        )