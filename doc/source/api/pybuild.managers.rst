pybuild.managers package
========================

Submodules
----------

pybuild.managers.artifactory module
-----------------------------------

.. automodule:: pybuild.managers.artifactory
    :members:
    :undoc-members:
    :show-inheritance:

pybuild.managers.bamboo module
------------------------------

.. automodule:: pybuild.managers.bamboo
    :members:
    :undoc-members:
    :show-inheritance:

pybuild.managers.confluence module
----------------------------------

.. automodule:: pybuild.managers.confluence
    :members:
    :undoc-members:
    :show-inheritance:

pybuild.managers.ftp module
---------------------------

.. automodule:: pybuild.managers.ftp
    :members:
    :undoc-members:
    :show-inheritance:

pybuild.managers.jira module
----------------------------

.. automodule:: pybuild.managers.jira
    :members:
    :undoc-members:
    :show-inheritance:

pybuild.managers.ldap_optiver module
------------------------------------

.. automodule:: pybuild.managers.ldap_optiver
    :members:
    :undoc-members:
    :show-inheritance:

pybuild.managers.subversion module
----------------------------------

.. automodule:: pybuild.managers.subversion
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: pybuild.managers
    :members:
    :undoc-members:
    :show-inheritance:
