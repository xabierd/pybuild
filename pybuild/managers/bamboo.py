from ..common import initialize_logger, RestAPIClient, class_repr
log = initialize_logger(__name__)

from ..conf import settings

import json
import sys

class BambooClient(RestAPIClient):
    """
    Interaction with Bamboo API using either OAuth or supplied auth.
    You could use either OAuth or (user,pass) when initializing
    """
    def __init__(self, url=settings.BAMBOO_URL, auth=None):
        if not auth:
            auth = self.get_auth('bamboo')
        RestAPIClient.__init__(self, url, auth=auth)
        self._result = '/rest/api/latest/result'
        self._info = '/rest/api/latest/info'

    def __repr__(self):
        return class_repr((vars(self)))

    def get_server_version(self):
        """
        :return: Bamboo server version in format: X.Y.Z-build-NNNN
        """
        url = self._info + '.json'
        resp = self.rest_get(url)
        if resp.status_code == 200:
            info = json.loads(resp.text)
            return "{}-build-{}".format(info['version'], info['buildNumber'])
        else:
            log.error("Error retrieving info: {} {}".format(resp.status_code, resp.text))
            sys.exit(-1)

    def get_build_report(self, key, build_number):
        """
        :param key: Either build or job key
        :param build_number: Bamboo build number
        :return: JSON with build/job info
        """
        url = '/'.join([self._result, key, str(build_number) + '.json'])
        resp = self.rest_get(url)
        if resp.status_code == 200:
            return json.loads(resp.text)
        else:
            log.error("Error retrieving build/job info: {} {}".format(resp.status_code, resp.text))
            sys.exit(-1)