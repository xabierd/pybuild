from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)

from pybuild.tasks.promote import Promote

class PromoteML(Promote):
    """Marketlinks build promotion in Artifactory
    """

    def __init__(self):
        Promote.__init__(self)

    @staticmethod
    def configure_parser(parser):
        Promote.configure_parser(parser)

    def configure(self, bamboo=True):
        Promote.configure(self, bamboo=bamboo)

    def do(self, parser):
        Promote.do(self, parser)
