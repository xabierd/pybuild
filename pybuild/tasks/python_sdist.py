from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)

from pybuild.conf import settings

from pybuild.tasks.task import Task
from pybuild.managers import Artifactory, BambooClient, ConfluenceClient, JIRAClient, Svn, Ftp, LDAPOptiver
from pybuild.build import Build

import os

class PythonSdist(Task):
    """Main build tasks: CMake, make, ctest, lcov and doxygen
    """

    def __init__(self):
        Task.__init__(self)
        self.build = None

    @staticmethod
    def configure_parser(parser):
        """ Parser for generic build task
        """
        Task.configure_parser(parser)

    def configure(self):
        Task.configure(self)
        self.build = Build(bamboo=BambooClient())
        self.build.init_vcs()

    def do(self, parser):
        Task.do(self, parser)

        self.build.python_sdist()
