from tests import *

from pybuild.managers.ftp import Ftp

class TestFtp(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.ftp = Ftp()
    def tearDown(cls):
        #icls.ftp.cd('/pub/DS/test')
        #icls.ftp.pwd()
        #icls.ftp.rmd('resources')
        pass

    def test_basics(self):
        log.info('LS:\nRemote path: %s. Listing: %s' % (self.ftp.pwd(), self.ftp.nlst()))

    def test_mkdir(self):
        self.ftp.cwd('/pub/DS')
        self.ftp.cwd('test', create=True)
        self.ftp.mkd('bla')
        self.ftp.mkd('foo3')
        self.ftp.mkd('bla')
        self.ftp.mkd('one/two/three/four')
        log.info('LS:\nRemote path: %s. Listing: %s' % (self.ftp.pwd(), self.ftp.nlst()))
        #self.ftp.cwd('../../../')
        self.ftp.cwd('/pub/DS/test')
        log.info('LS:\nRemote path: %s. Listing: %s' % (self.ftp.pwd(), self.ftp.nlst()))
        self.ftp.mkd('one/twoandahalf')

    def test_put(self):
        print os.listdir('.')
        self.ftp.put('README', '/pub/DS/test')
        print self.ftp.pwd()
        self.ftp.put('tests/resources', '/pub/DS/test')
        self.ftp.cwd('../../..')
        print self.ftp.pwd()
        self.ftp.cwd('/pub/DS/test')
        print self.ftp.pwd()
        self.ftp.rmd('bla')
