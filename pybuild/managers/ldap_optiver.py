from ..common import initialize_logger, get_auth
log = initialize_logger(__name__)

from ..conf import settings

import ldap

class LDAPOptiver(object):
    def __init__(self):
        self.auth = get_auth('ldap')
        self.uri = settings.LDAP_URL
        self.base = 'DC=comp, DC=optiver, DC=com'
        self.ad = ldap.initialize(self.uri)
        self.ad.simple_bind_s(self.auth[0], self.auth[1])


    def get_user(self, username):
        result_id = self.ad.search(self.base, ldap.SCOPE_SUBTREE, 'sAMAccountName=' + username)
        try:
            result = self.ad.result(result_id, 0)[1][0][1]
        except IndexError, e:
            log.error('User "%s" not found' % username)
            raise IndexError('User "%s" not found' % username)
        return result

