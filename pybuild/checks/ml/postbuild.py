from pybuild.common import initialize_logger, log_header, multiple_replace
log = initialize_logger(__name__)

from pybuild.checks import PostBuildChecks as GenericPostBuildChecks

class PostBuildChecks(GenericPostBuildChecks):

    @classmethod
    def setUpClass(cls):
        super(PostBuildChecks, cls).setUpClass()
    
    def test_specific_marketlinks(self):
        """Check specific to ML projects"""
        self.assertTrue(True)
        self.assertFalse(False)