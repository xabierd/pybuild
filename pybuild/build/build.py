"""
Main build pipeline object.

Inherits all it's attributes from :class:`BuildConfig`

"""
from pybuild.common import initialize_logger, run_command, log_header, number_of_cores, get_sysinfo, copy, clean_folders
log = initialize_logger(__name__)

from pybuild.conf import settings

from datetime import datetime
import time
import shutil
import os
import sys
import json
import pkg_resources


from buildConfig import BuildConfig
from pybuild.managers import Ftp, externals_to_svn17


GENERATOR_VS12_x64 = 'Visual Studio 12 Win64'
WIN32 = 'win32' in sys.platform


def read_func(name, mode):
    # For unit test reading files
    return open(name, mode=mode).read()

class Build(BuildConfig):
    """
    This class represents a build pipeline and contains methods executing
    basic build tasks.
    Attributes are inherited from it's base class, :class:`BuildConfig`

    """
    def __init__(self, config=None, bamboo=None, artifactory=None,
                 vcs=None, jira=None, confluence=None):
        BuildConfig.__init__(self, config, bamboo=bamboo, artifactory=artifactory,
                             vcs=vcs, jira=jira, confluence=confluence)


    def generate_buildinfo_data(self):
        """
        Generates the data needed for common_cpp/libs/buildinfo as per `BuildInfo specs <http://devwiki.ams.optiver.com/display/DSAMS/BuildInfo+specs>_`
        :return: dictionary used to pass to run_command
        """
        return {
            'BUILDINFO_BUILD_NUMBER'   : self.buildNumber,
            'BUILDINFO_BUILD_VERSION'  : self.get_build_version(),
            'BUILDINFO_PRODUCT_NAME'   : self.productName,
        }

    def export_buildinfo_data(self, env):
        """
        Exports the build environment and system info into a JSON file.
        This is used when publishing the build to Artifactory, to populate Artifactory's build Info.
        It should be called from the task that actually builds the release artifact,
        otherwise the information stored will be incorrect (not where tha artifact was built)
        """
        environment_info = dict()
        env_merge = os.environ.copy()
        env_merge.update(env)
        for v in env_merge:
            environment_info['buildInfo.env.' + v] = env_merge[v]

        system = settings.SYSTEM_INFO['windows'] if WIN32 else settings.SYSTEM_INFO['linux']
        build_info = environment_info.copy()
        build_info.update(get_sysinfo(system))
        with open(settings.BUILD_INFO_ARTIFACT, 'w') as outfile:
                json.dump(build_info, outfile)

    def import_buildinfo_data(self):
        """
        Reads Build Info data from JSON file and puts it into a dictionry
        :return: Dictionary with data ready for Artifactory build Info
        """
        log.info("Importing Artifactory buildInfo data from %s" % settings.BUILD_INFO_ARTIFACT)
        try:
            with open(settings.BUILD_INFO_ARTIFACT) as data_file:
                data = json.load(data_file)
            return data
        except IOError:
            log.warning('No build Info to publish to Artifactory')
            return dict()

    def run_cmake(self, build_dir=settings.DEFAULT_BUILD_DIR, src_dir=settings.DEFAULT_SRC_DIR, build_debug=False, cmake_args=''):
        """
        Run ``cmake`` command

        :param build_dir: build directory
        :param src_dir: source code directory
        :param build_debug: boolean used to get the value for ``-DCMAKE_BUILD_TYPE``
        :param cmake_args: extra arguments to ``cmake``
        """
        log.info(log_header() % 'Running "cmake"')
        clean_folders([self.artifactsDir, self.reportsDir, self.buildDir])
        generator = GENERATOR_VS12_x64 if WIN32 else ''
        build_type = '-DCMAKE_BUILD_TYPE=Debug' if build_debug else '-DCMAKE_BUILD_TYPE=RelWithDebInfo'
        env = self.generate_buildinfo_data()
        if os.path.isdir(build_dir): shutil.rmtree(build_dir)
        os.makedirs(build_dir)
        try:
            run_command("cd %s && cmake ../%s -DCMAKE_INSTALL_PREFIX:PATH=./" % (build_dir, ' '.join([src_dir, build_type, generator, ' '.join(cmake_args)])),
                        env=env)
        except Exception as ex:
            log.error("CMake failed: %s", str(ex))
            sys.exit(-1)

    def run_cmake_build(self, build_dir=settings.DEFAULT_BUILD_DIR, build_debug=False):
        """
        Run ``cmake --build`` command

        :param build_dir: build directory
        :param build_debug: boolean used to get the value for ``-DCMAKE_BUILD_TYPE``
        """
        nc = number_of_cores()
        log.info(log_header() % 'Running "cmake --build"')
        build_type = 'Debug' if build_debug else 'RelWithDebInfo'
        env = self.generate_buildinfo_data()
        self.export_buildinfo_data(env)
        try:
            if WIN32: run_command("cd %s && cmake --build . --config %s -- /m:%i"%(build_dir, build_type, nc), env=env)
            else: run_command("cd %s && cmake --build . --config %s -- -j%i"%(build_dir, build_type, nc), env=env)
        except Exception as ex:
            log.error("make failed: %s", str(ex))
            sys.exit(-1)

    def run_make_install(self, build_dir=settings.DEFAULT_BUILD_DIR):
        """
        Run ``make install`` command

        :param build_dir: build directory
        """
        nc = number_of_cores()
        log.info(log_header() % 'Running "make install"')
        env = self.generate_buildinfo_data()
        self.export_buildinfo_data(env)
        try:
            if WIN32: run_command("cd %s && make install -j%i" % (build_dir, nc), env=env)
            else: run_command("cd %s && make install -j%i" % (build_dir, nc), env=env)
        except Exception as ex:
            log.error("make install failed: %s", str(ex))
            sys.exit(-1)

    def run_generic(self, command, cwd=None):
        """
        Run a generic shell command

        :param command: string with command to run
        :param: cwd: directory to run the command into, ie: will `cd` into it
        """
        log.info(log_header() % 'Running "%s"' % command)
        env = self.generate_buildinfo_data()
        try:
            if cwd:
                run_command("cd %s && %s" % (cwd, command), env=env)
            else:
                run_command(command, env=env)
        except Exception as ex:
            log.error("%s failed: %s" % (command.split(' ')[0], str(ex)))
            sys.exit(-1)

    def rename_binary_with_version(self, binary):
        """
        Renames a file appending the current product version, ie: `file.txt` will be renamed to `file-1.2.3.txt`

        :param binary: path and name of the file to rename
        """
        filename, extension = os.path.splitext(binary)
        new_name = '%s-%s' %(filename,self.get_build_version())
        if extension :
            new_name += '.%s' % extension
        try:
            log.info('Renaming binary at "%s" to "%s"' % (binary, new_name))
            os.rename(binary, new_name)
        except Exception, e:
            log.error('Error renaming from %s to %s' % (binary, new_name))
            sys.exit(-1)

    def collect_artifacts_from_folder(self, from_folder, to_folder=None, to_file=None, create_zip=True):
        """Creates zip with the contents of the 'from_folder' and places it into the artifacts folder

        :param from_folder: folder to pick the contents from
        :param to_folder: folder to place the zip file into. Defaults to ``build.artifactsDir``
        """
        if not to_folder:
            to_folder = self.artifactsDir
        if not to_file:
            to_file = settings.DEFAULT_ARTIFACT_NAMING_PATTERN.\
            replace('{product}', self.productName).\
            replace('{version}', self.get_build_version()).\
            replace('{build}', self.buildNumber).\
            replace('{revision}', self.short_revision)
        to_file = os.path.join(to_folder, to_file)
        log.info(log_header() % 'Collecting artifacts from: %s', from_folder)
        # if os.path.isdir(to_folder): shutil.rmtree(to_folder)
        if create_zip:
            shutil.make_archive(to_file,
                                format='zip',
                                root_dir=from_folder)
            log.info('Artifact created at: %s.zip' % to_file)
        else:
            copy(from_folder, to_folder)

    def create_build_info(self, artifacts=list()):
        """
        Returns a build info dictionary which is formatted to correctly deploy a new build to artifactory.
        Make a put request with this build info to api/build
        """
        bamboo_version = self.bamboo.get_server_version()
        duration = int(self.bamboo.get_build_report(self.buildKey, self.buildNumber)['buildDuration'])
        build_info = dict(properties=self.import_buildinfo_data(),
                          version='1.0.1',
                          name= self.planName,  #  self.buildName,  #
                          number=str(self.buildNumber),
                          type='GENERIC',
                          agent=dict(name='Bamboo', version=bamboo_version),
                          buildAgent=dict(name='pyBuild', version=str(pkg_resources.get_distribution('pybuild')).split()[1]),
                          # TODO: Pick the right time from Bamboo
                          started=datetime.now().strftime('%Y-%m-%dT%H:%M:%S.000-0000'),
                          durationMillis=duration,
                          principal=self.get_build_user(),
                          artifactoryPrincipal='bamboo',
                          url='http://bamboo.ams.optiver.com/browse/{}-{}'.format(self.buildKey, self.buildNumber),
                          vcsRevision=self.vcsRevision,
                          vcsUrl=self.vcsURL,
                          modules=[{'id': '{}:{}'.format(self.planName, self.buildNumber),
                                    'artifacts': artifacts,
                                   }])
        return build_info

    def deploy_folder(self, artifactory_repo=None,
                      artifact_remote_path=None,
                      local_folder=settings.DEFAULT_ARTIFACTS_DIR,
                      parameters=None):
        """
        Uploads the contents of specified folder to Artifactory

        :param artifactory_repo: Artifactory repo to upload to
        :param artifact_remote_path: Artifactory path to upload to
        :param local_folder: folder to upload
        :param parameters: additional parameters to add to the upload files
        """
        if not artifactory_repo:
            artifactory_repo=self.artifactoryCIRepo
        if not artifact_remote_path:
            artifact_remote_path=self.get_artifactoryProductPath()
        #if not naming_pattern:
        #    naming_pattern = settings.DEFAULT_ARTIFACT_NAMING_PATTERN

        params = {
            "build.name": self.planName,  #self.buildName, #
            "build.job": self.buildName,
            "build.timestamp": int(time.time()),  # iso8601_to_epoch(bamboo_job["buildCompletedTime"]),
            "build.number": self.buildNumber,
            "vcs.url": self.vcsURL,
            "vcs.revision": self.vcsRevision,
            "version": self.get_build_version(),
        }
        # Concatenate the parameters and the basic params, overwriting the basic with the provided
        if parameters:
            for d in parameters:
                params[d] = parameters[d]

        remote_path = '/'.join(['', artifactory_repo, artifact_remote_path])
        if not os.path.isdir(local_folder):
            log.error('Local folder {folder} does not exist'.format(folder=local_folder))
            sys.exit(-1)
        else:
            artif_buildinfo = []
            if True in [os.path.isdir(d) for d in os.listdir(local_folder)]:
                log.error('Can not deploy nested folders inside {}'.format(local_folder))
                sys.exit(-1)
            for f in os.listdir(local_folder):
                dest_path = '/'.join([remote_path, f])
                full_local_path = os.path.join(local_folder, f)
                log.info('Publishing {local} to {remote}'.format(local=full_local_path, remote=dest_path))
                artif_buildinfo.append(self.artifactory.upload(dest_path, full_local_path, params))
                log.info("Artifact {artifact} deployed to {path} with parameters {params}".
                         format(artifact=f, path=dest_path, params=params))

            build_buildinfo = self.create_build_info(artif_buildinfo)
            log.info('Creating build "%s:%s" in Artifactory' % (build_buildinfo['name'], build_buildinfo['number']))
            self.artifactory.create_build(build_buildinfo)

    def promote(self, build_name, build_number, target_repo, status, comment, dry_run="false"):
        """
        Promotes a build in Artifactory. Effectively, it will copy the build from one repo to another and
        mark the build as promoted with the corresponding comment.

        :param build_name: name of the build to promote
        :param build_number: build number of the build to promote
        :param target_repo: Artifactory repo to promote the build to
        :param status: New build status, ie: "production"
        :param comment: Displayed in Artifactory/Build/Release History
        :param dry_run: dry run
        :return: Results of the post REST request to Artifactory
        """
        if not target_repo: target_repo = settings.DEFAULT_RELEASES_REPO
        params = {
            "status": status,  # new build status (any string)
            "comment": comment,
            # An optional comment describing the reason for promotion. Default: ""
            "ciUser": self.get_build_user(),  # The user that invoked promotion from the CI server
            # TODO: Time offset. Regex: r'(?P<Y>^\d{4})-(?P<m>\d{2})-(?P<d>\d{2})T(?P<H>\d{2}):(?P<M>\d{2}):(?P<S>\d{2}).(?P<f>\d{3})(?P<sign>\+|-)(?P<oh>\d{2}):(?P<om>\d{2})'
            "timestamp": datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.000-0000'),
            # the time the promotion command was received by Artifactory
            "dryRun": dry_run,  # run without executing any operation in Artifactory,
            # but get the results to check if the operation can succeed. Default: false
            "targetRepo": target_repo,  # optional repository to move or copy the build's artifacts and/or dependencies
            "copy": "true",  # whether to copy instead of move, when a target repository is specified. Default: false
            "artifacts": "true",  # whether to move/copy the build's artifacts. Default: true
            "dependencies": "false",  # whether to move/copy the build's dependencies. Default: false.
            #"scopes": ["compile", "runtime"],
            # an array of dependency scopes to include when "dependencies" is true
            #"properties": {
            #    "release-name": ["test-1.2.3"]
            #},
            "failFast": "true"  # fail and abort the operation upon receiving an error. Default: true
        }
        res = self.artifactory.promote_build(build_name, build_number, params)
        log.info("Build {key}-{buildNumber} successfully promoted to {repo}".
                 format(key=self.planKey, buildNumber=build_number, repo=target_repo))
        return res


    def publish_rns(self, path, content=None, space_key='RN'):
        """
        Publish release notes to Confluence

        :param path: Path where to publish the notes. At least the first element of the path has to exist
        :param content: Content to publish, if not provided, defaults to contents of ``RELEASE_NOTES.txt``
        :param space_key: Space where to publish. Defaults to `RN`
        :return:
        """
        log.info("Publishing Release Notes to Confluence")
        if not self.confluence:
            log.error('Confluence has not been initialized')
            sys.exit(-1)

        if not content:
            content = read_func(os.sep.join(['RELEASE_NOTES.txt']), mode='rb')

        parent = path.split('/')[0]
        rest =  path.split('/')[1:]


        page_id = self.confluence.create_page_tree(space_key=space_key, parent_title=parent,
                                                   page_tree=rest, content=content)

        if not  page_id:
            log.error('Error creating page tree in Confluence')
            sys.exit(-1)
        return True

    def publish_to_ftp(self, reports_folder, remote_folder=None):
        """
        Uploads reports to FTP share

        :param reports_folder: local folder with the contents to upload
        :param remote_folder: Defaults to ``pub/artifactory_path/product/version/build_number``
        """
        if not remote_folder:

            remote_ftp_folder = '/'.join(['pub', self.get_artifactoryProductPath(),
                                          'build{0:04d}'.format(int(self.buildNumber))])
        else:
            remote_ftp_folder = remote_folder

        log.info('Uploading reports to %s' % remote_ftp_folder)
        ftp = Ftp()
        ftp.mkd(remote_ftp_folder)
        ftp.put(reports_folder, remote_ftp_folder)

    def annotate(self, repo, properties):
        self.artifactory.set_artifact_properties(repo, self.artifactoryPath, self.productName, self.get_build_version(), properties)

    def export_pinned_svn_externals(self, out_file):
        if self.repo_type == 'svn':
            externals = externals_to_svn17(self.vcs.pinned_externals())
            with open(out_file, 'w+') as fl:
                    fl.write(externals)
        else:
            log.error('Can not export externals for non svn repositories')

    def python_sdist(self):
        """
        Creates a Python source distribution in zip format and versioned includding
        :return:
        """
        env = self.generate_buildinfo_data()
        self.export_buildinfo_data(env)

        clean_folders([self.artifactsDir, self.reportsDir, self.buildDir], create=False)

        self.export_buildinfo_data(env)

        version_file = os.path.join(settings.DEFAULT_SRC_DIR, 'VERSION')
        with open(version_file, 'w+') as version:
            version.write(self.get_build_version())


        run_command("cd {source} && python setup.py sdist --dist-dir=../{build} --formats=zip".format(
            source=settings.DEFAULT_SRC_DIR,
            build=settings.DEFAULT_ARTIFACTS_DIR))


        # Add dev{buildNumber} to binary
        binary = os.path.join(settings.DEFAULT_ARTIFACTS_DIR,
                              '{prod}-{version}.zip'.format(prod=self.productName,
                                                            version=self.get_build_version()))
        filename, extension = os.path.splitext(binary)
        new_name = '{filename}.dev{build}{extension}'.\
            format(
                    filename=filename,
                    build=self.buildNumber,
                    extension=extension
                )
        try:
            log.info('Renaming binary at "%s" to "%s"' % (binary, new_name))
            os.rename(binary, new_name)
        except Exception, e:
            log.error('Error renaming from %s to %s' % (binary, new_name))
            sys.exit(-1)

