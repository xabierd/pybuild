from pybuild.common import initialize_logger, log_header, unzip, clean_folders
log = initialize_logger(__name__)

from pybuild.conf import settings

from pybuild.tasks.task import Task
from pybuild.managers import Artifactory, BambooClient, ConfluenceClient, JIRAClient, Svn, Ftp, LDAPOptiver
from pybuild.build import Build

import time
import os
import shutil
from glob import glob

class Promote(Task):
    """Promote a build in Artifactory
    """
    def __init__(self):
        Task.__init__(self)
        self.build = None

    @staticmethod
    def configure_parser(parser):
        """ Parser for generic promotion task
        """
        Task.configure_parser(parser)

        parser.add_argument('--promote-to-folder', help="If provided, will copy the release to this folder")
        parser.add_argument('--no-artifactory', action="store_true", help="Ignore Artifactory promotion")
        parser.add_argument('-t', '--target-repo', dest='target_repo', help='Target repo to promote to')
        parser.add_argument('-a', '--artifactory-path', dest='artifactory_path', help='Full path to deploy to in Artifactory, ie: ML/bin/product/1.2.3')
        parser.add_argument('-s', '--status', help='Status to apply to the build in Artifactory, ie: Released')
        parser.add_argument('-c', '--comment', help='Comment in Artifactory\'s release history')
        parser.add_argument('-r', '--publish-reports', action='store_true', help='Publish reports from  reports folder "artifacts/reports"')
        parser.add_argument('-d', '--dry-run', dest='dry_run', help='Dry run')

        parser.set_defaults(build_name=None,
                            build_number=None,
                            target_repo=settings.DEFAULT_RELEASES_REPO, # Default target is releases repo
                            artifactory_path=None,                      # Default is taken from bamboo_artifactory_path
                            status=settings.DEFAULT_PROMOTION_STATUS,
                            comment=None,
                            dry_run=False,
                            promote_to_folder='',
                            publish_reports=False
        )

    def configure(self, bamboo=True):
        Task.configure(self)
        self.build = Build(bamboo=BambooClient(), artifactory=Artifactory())
        self.build.init_vcs()

    def do(self, parser):
        Task.do(self, parser)
        self.configure()

        # Clean folder to collect reports
        clean_folders([self.build.reportsDir])

        # If --no-artifactory flag has not been set (default)
        # This is intended to be used in an emergency, in case Artifactory is down
        if not self.args.no_artifactory:

            # Metadata to add to artifact in Artifactory
            if self.build.vcsRepoType == 'svn':
                vcs_tag = '/'.join([self.build.vcs.tags_url, self.build.full_version_tag])
            else:
                vcs_tag = self.build.full_version_tag

            properties = {'vcs.tag': vcs_tag,
                          'release.date': time.strftime('%d/%m/%Y'),
                          'release.time': time.strftime('%H:%M:%S'),
                          'release.user': self.build.get_build_user()}

            if self.args.publish_reports:
                # Collect artifacts from 'artifacts/reports' folder and place them in 'reports' folder
                # Subfolders are zipped (that's why we need to collect)
                reports_content = os.listdir(os.path.join(self.build.artifactsDir, self.build.reportsDir))
                if reports_content:
                    log.info('Collecting reports')
                    for f in reports_content:
                        f_path = os.path.join(self.build.reportsDir, f)
                        if os.path.isdir(f_path):
                            log.debug('Creating zip with files in %s' % f_path)
                            self.build.collect_artifacts_from_folder(f_path, self.build.reportsDir, f)
                        else:
                            log.debug('Collecting %s' % f)
                            shutil.copy(f_path, os.path.join(self.build.reportsDir, f))
                        log.info('Publishing reports to Artifactory')
                        self.build.deploy_folder(self.args.target_repo, self.args.artifactory_path, self.build.reportsDir, parameters=properties)
                else:
                    log.warning('No reports to publish!!')

            log.info('Promoting build "%s" in Artifactory' % self.build.planName)
            comment= settings.DEFAULT_PROMOTION_COMMENT.format(user=self.build.get_build_user()) \
                if self.args.comment is None else self.args.comment

            # TODO: Convert ' - Publish' in an optional argument
            self.build.promote(self.build.planName,  # + ' - Publish',
                               self.build.buildNumber,
                               self.args.target_repo,
                               self.args.status,
                               comment,
                               self.args.dry_run)

            self.build.annotate(self.args.target_repo, properties)

        if self.args.promote_to_folder:
            # Will copy the shared Bamboo artifact, so no dependency on Artifactory
            # Assume artifact is in artifacts/binaries folder and RNs in artifacts/reports

            rns_file = os.path.join(settings.DEFAULT_ARTIFACTS_DIR, 'reports', 'RELEASE_NOTES.txt')
            binaries_folder = os.path.join(self.build.artifactsDir, 'binaries')

            # Unzip files in artifacts folder
            zipfiles = glob(os.path.join(binaries_folder, '*.zip'))
            for zfile in zipfiles:
                log.debug('Unzipping %s' % zfile)
                unzip(zfile, binaries_folder)
                os.remove(zfile)

            # Copy RNs to artifacts folder
            shutil.copy(rns_file, binaries_folder)

            # Copy artifacts folder to promotion share (provided as argument)
            destination = os.path.join(self.args.promote_to_folder,
                                       self.build.productName,
                                       self.build.full_version_tag)
            log.info('Copying contents of "%s" to "%s"' % (settings.DEFAULT_ARTIFACTS_DIR, destination))
            shutil.copytree(binaries_folder, destination)

