"""
Artifactory manager deals with Artifactory interaction.

.. moduleauthor:: Xabier Davila <xabierdavila@optiver.com>

"""
from ..common import initialize_logger, RestAPIClient, class_repr
log = initialize_logger(__name__)

from ..conf import settings

from artifactory_fs import ArtifactoryPath

import simplejson
import sys, os

_CONTENT_TYPE_PROMOTION_REQUEST = 'application/vnd.org.jfrog.artifactory.build.PromotionRequest+json'
_CONTENT_TYPE_PROMOTION_RESULT = 'application/vnd.org.jfrog.artifactory.build.PromotionResult+json'
_CONTENT_TYPE_PUBLISH_BUILD_INFO = 'application/vnd.org.jfrog.artifactory+json'


class Artifactory(RestAPIClient):
    """
    Object to interact with Artifactory's REST API
    """
    def __init__(self, url=settings.ARTIFACTORY_URL, usr=None, pwd=None):
        if not usr:
            usr, pwd = self.get_auth('artifactory')
        self.auth = (usr, pwd)

        RestAPIClient.__init__(self, url, usr=usr, pwd=pwd)

        self._build = '/api/build'
        self._promoteBuild = '/api/build/promote/{build_name}/{build_number}'
        self._latestVersion = '/api/search/latestVersion?g={group}&a={name}&repos={repo}'
        self._versions = '/api/search/versions?g={group}&a={name}&repos={repo}'
        self._latest_release = '/{releases_repo}/{path}/{prod_name}/[RELEASE]/{prod_name}-[RELEASE].zip'
        self._latest_integration = '/{ci_repo}/{path}/{prod_name}/{version}/{prod_name}-{version}+[INTEGRATION].zip'
        self._set_properties = '/api/storage/{repo}/{full_path}?properties={properties}'


    def __repr__(self):
        return class_repr(vars(self))

    def artifact_exists(self, path):
        """
        Check if an artifact exists

        :param path: path to artifact, including repo segment, but not the server URL up to 'artifactory'
        :return:  True/False
        """
        ap = ArtifactoryPath(self.server_url + path, auth=self.auth)
        return ap.is_file()

    def upload(self, artifactory_path, origin_path, parameters=None):
        """
        Single file upload to Artifactory

        :param artifactory_path: Artifactory relative URL, including filename
        :param origin_path: file path relative to script path
        :param parameters: dictionary with additional properties to add to the artifact
        :return: on success, returns dict with artifact properties
        """
        ap = ArtifactoryPath(self.server_url + artifactory_path, auth=self.auth)
        ap.deploy_file(origin_path, parameters=parameters)
        return ap.info()


    def download_by_path(self, artifactory_path, destination_path='./'):
        """
        Download artifact by its path

        :param artifactory_path: path of the artifact
        :param destination_path: local path where to store the downloaded file
        """
        log.debug('Downloading artifact %s' % artifactory_path)
        ap = ArtifactoryPath(self.server_url + artifactory_path, auth=self.auth)

        with ap.open() as fd:
            with open(destination_path, "wb") as out:
                out.write(fd.read())

    def create_build(self, build_info):
        """
        Create a build in Artifactory

        :param build_info: JSON containing the build information
        """
        url = self._build
        headers = {'Content-Type': _CONTENT_TYPE_PUBLISH_BUILD_INFO,
                   'Accept': 'text/plain'}
        log.debug('\n%s' % build_info)
        resp = self.rest_put(url, data=simplejson.dumps(build_info, sort_keys=True), headers=headers)
        if resp.status_code != 204:
            log.error('Error creating build {} {}'.format(resp.status_code, resp.text))
            sys.exit(-1)
        log.info('Created build {}'.format(build_info['name']))


    def promote_build(self, build_name, build_number, params,):
        """
        Promotes a build in Artifactory

        :param build_name: build name
        :param build_number: build number
        :param params: additional parameters
        :return: response from Artifactory
        """
        url = self._promoteBuild.format(build_name=build_name, build_number=build_number)
        headers = {'Content-Type': _CONTENT_TYPE_PROMOTION_REQUEST,
                   'Accept': _CONTENT_TYPE_PROMOTION_RESULT}
        #res = requests.post(self.server_url + url,
        #                    headers=headers,
        #                    data=simplejson.dumps(params, sort_keys=True),
        #                    auth=self.auth)
        res = self.rest_post(url=url, params=params, headers=headers, data=simplejson.dumps(params, sort_keys=True))
        if res.status_code != 200:
            log.error('Error promoting build: {}: {}'.format(res.status_code, res.text))
            sys.exit(-1)
        return res

    def get_latest_version(self, repo, path, name, release_repo=False):
        """
        Gets the latest version for a given group/module

        :param path: group ID, ie: ML/lib
        :param name: product name, ie: ml_mlpie_test
        :return: string with latest version
        """
        log.debug('Retrieving last version for {repo}/{group}/{name}'.format(
            group=path, name=name, repo=repo))
        url = self._latestVersion.format(group=path, name=name, repo=repo)
        url = url + '&v=*' if not release_repo else url
        log.debug(url)
        resp = self.rest_get(url=url, headers={'Accept': 'text/plain'})
        if resp.status_code == 404:
            log.error('Path does not exist')
        elif resp.status_code != 200:
            log.error('Error retrieving latest version: %s' % resp.text)
        else:
            return resp.text

    def get_versions(self, repo, path, product_name):
        """
        Retrieves JSON with existing version for a given repo, path and product name.
        Results are in the format:

            {
              "results" : [ {
                "version" : "5.2.0-beta009+build043",
                "integration" : true
              }, {
                "version" : "5.2.0-beta009+build042",
                "integration" : true
              } ]
            }

        """

        log.debug('Retrieving versions for {repo}/{group}/{name}'.format(
            group=path, name=product_name, repo=repo))
        url = self._versions.format(group=path, name=product_name, repo=repo)
        log.debug(url)
        resp = self.rest_get(url=url)
        if resp.status_code == 404:
            log.error('Path does not exist')
        elif resp.status_code != 200:
            log.error('Error retrieving versions: %s' % resp.text)
        else:
            return resp.text

    def download_latest_release(self, remote_path, prod_name, releases_repo=settings.DEFAULT_RELEASES_REPO,
                                local_path='', local_file=None):
        """
        Downloads the latest *release* version for a given path/product

        :param remote_path: Artifactory path, ie: ML/bin
        :param prod_name: artifact's name, ie: mlp_test_ie
        :param releases_repo: optional, defaults to default releases repo specified in settings
        :param local_path: optional, path where to store the artifact
        :param local_file: optional, defaults to current artifact name, ie: foo-1.2.3.zip
        """
        url = self.server_url + self._latest_release.format(releases_repo=releases_repo, path=remote_path, prod_name=prod_name)
        log.debug('Downloading release artifact: %s' % url)
        ap = ArtifactoryPath(url, auth = self.auth)
        bf, filename = ap.open()
        dest_file = local_file if local_file else filename
        dest_full_path = os.path.join(local_path,dest_file)
        log.info('Downloading artifact: %s to %s' % (filename, dest_full_path))
        try:
            with open(dest_full_path, 'wb') as out:
                out.write(bf.read())
        except Exception, e:
            log.error('Some error')
            print e

    def download_latest_integration(self, remote_path, prod_name, version, ci_repo=settings.DEFAULT_CI_REPO,
                                    local_path='', local_file=None):
        """
        Downloads the latest *integration* version for a given path/product

        :param remote_path: Artifactory path, ie: ML/bin
        :param prod_name: artifact's name, ie: foo
        :param version: short version, ie: 1.2.3_beta1
        :param ci_repo: optional, defaults to default CI repo specified in settings
        :param local_path: optional, path where to store the artifact
        :param local_file: optional, defaults to current artifact name, ie: foo-1.2.3_beta1+build999.zip
        """
        url = self.server_url + self._latest_integration.format(ci_repo=ci_repo, path=remote_path, prod_name=prod_name,
                                                                version=version)
        log.debug('Downloading integration artifact: %s' % url)
        ap = ArtifactoryPath(url, auth = self.auth)
        bf, filename = ap.open()
        dest_file = local_file if local_file else filename
        dest_full_path = os.path.join(local_path,dest_file)
        log.info('Downloading artifact: %s to %s' % (filename, dest_full_path))
        try:
            with open(dest_full_path, 'wb') as out:
                out.write(bf.read())
        except Exception, e:
            log.error('Some error')
            print e

    def set_artifact_properties(self, repo, path, artifact, version, properties, recursive=True):
        """
        Annotates an artifact with the provided properties

        :param repo: Artifactory repo
        :param path: artifact path
        :param artifact: artifact name
        :param version: artifact version
        :param properties: dictionary with the properties to apply
        :param recursive: if True, properties will be applied recursively
        """

        props = '|'.join(['%s=%s' % (k, properties[k])  for k in properties])
        full_path = '/'.join([path, artifact, version])
        url = self._set_properties.format(repo=repo, full_path=full_path, properties=props)
        if recursive:
            url += '&recursive=1'
        print repo, path , artifact, version, properties
        log.debug('Set props URL: %s' % url)
        res = self.rest_put(url=url)
        if res.status_code != 204:
            log.error('Could not set artifact properties "%s"\n%s' % (url, res.text))
            sys.exit(-1)

