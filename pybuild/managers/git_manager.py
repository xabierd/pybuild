from ..common import initialize_logger, class_repr
log = initialize_logger(__name__)

from ..conf import settings

from git import Repo
from stash import StashClient
import re
import sys

class Git(object):
    """
    Git wrapper class
    """

    def __init__(self, path, url=None, revision=None):


        self.url = url          #: Remote URL. Used to get tags and branches
        self.repo = Repo(path)  #: GitPython object to interact with the git repo

        self.stash_base = None
        self.stash_project = None
        self.stash_repo = None
        self.revision = revision

        self.parse_url()

        self.stash = StashClient(url=self.stash_base)

        revision = self.repo.commit() if not self.revision else self.revision

        self._tags = None
        self._branches = None


    def parse_url(self):
        if not self.url:
            if self.repo.remotes:
                self.url = self.repo.remotes[0].url
            else:
                log.error('Can\'t get remote URL for {0}'.format(self.repo))
                sys.exit(-1)
        match = re.search(settings.GIT_REMOTE_URL_REGEX, self.url)
        if match:
            self.stash_base    = 'http://' + match.groupdict()['base_url']
            self.stash_project = match.groupdict()['project']
            self.stash_repo    = match.groupdict()['repo']
        else:
            log.warning('Can\'t parse URL %s into Stash URL' % self.url)



    def __repr__(self):
        return class_repr(vars(self))

    def get_branches(self):
        """
        Returns a list with the repo branches

        :return: list of strings with branches names: ie: [ bar, releases/foo-1.2.x]
        """
        if not self._branches:
            self._branches = self.stash.get_branches(self.stash_project, self.stash_repo)
        return self._branches

    def get_release_branches(self):
        """
        Returns a list with the release branches

        :return: list of strings with release branches names
        """
        branches = self.get_branches()
        return  [b for b in branches if b.startswith(settings.GIT_RELEASE_BRANCHES)]

    def get_tags(self):
        """
        Returns a list with the tags

        :return: list of strings with tag names
        """
        if not self._tags:
            self._tags = self.stash.get_tags(self.stash_project, self.stash_repo)
        return self._tags

    def is_release_branch(self, branch_name=None, rev=None):
        branch_name = self.repo.active_branch if not branch_name else branch_name
        return branch_name in self.get_release_branches()

    def tag_exists(self, tag):
        return tag in self.get_tags()

    def create_tag(self, tag_name, message=None, revision=None):
        rev = self.revision if not revision else revision
        if message is None:
            message = settings.TAG_COMMIT_MSG.format(tag_name=tag_name)
        if self.tag_exists(tag_name):
            log.error("The tag '{tag_name}' already exists in {repo}".
                      format(tag_name=tag_name, repo=self.stash_repo))
            sys.exit(-1)
        else:
            if not self.stash.create_tag(self.stash_project, self.stash_repo, tag_name, message, rev):
                log.error('Error creating tag: {tag}, {rev}'.format(tag=tag_name, rev=revision))
                sys.exit(-1)




