"""Basic task class
"""

from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)


class Task(object):
    """Base task class
    """

    def __init__(self):
        self.name = self.__class__.__name__
        log.debug('Initializing "%s"' % self.name)
        self.parser = None
        self.args = None
        self.build = None

    @staticmethod
    def configure_parser(parser):
        """ Configure parser for this action
        """
        pass

    def configure(self):
        log.debug('Configuring task "%s"' % self.name)

    def do(self, parser):
        log.info(log_header() % 'Running "%s" task...' % self.name)
        self.parser = parser
        self.args = self.parser.parse_args()
