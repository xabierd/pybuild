"""Collection of classes that map to different  Build Tasks.

.. moduleauthor:: Xabier Davila <xabierdavila@optiver.com>

"""
# from task import Task

# Generic tasks
from prepare import Prepare
from prebuild import PreBuild
from build import Buildtask
from postbuild import PostBuild
from publish import Publish
from prepromote import PrePromote
from releasenotes import Releasenotes
from promote import Promote
from tag import Tag
from python_sdist import PythonSdist

# ML tasks
from ml.prepare import PrepareML
from ml.prebuild import PrebuildML
from ml.build import BuildtaskML
from ml.postbuild import PostbuildML
from ml.publish import PublishML
from ml.prepromote import PrePromoteML
from ml.releasenotes import ReleasenotesML
from ml.promote import PromoteML
from ml.tag import TagML
