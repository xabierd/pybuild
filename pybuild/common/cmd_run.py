"""
Command runner .

.. moduleauthor:: Xabier Davila <xabierdavila@optiver.com>

"""
from pybuild.common import initialize_logger
log = initialize_logger(__name__)

from pybuild.conf import settings

import sys
import os
import subprocess
import time

if settings.WIN32:
    pass
else:
    import fcntl

from threading import Thread
from Queue import Queue, Empty

output, errors = '', ''

def log_lines(str_buff, error=False):
    global output, errors
    if str_buff and str_buff.strip():
        for line in str_buff.split('\n'):
            line = line.strip()
            if line:
                if error:
                    log.error(' - %s' % line)
                    errors += '%s\n' % line
                else:
                    log.info(' - %s' % line)
                    output += '%s\n' % line

def add_buildinfo_environment(env):
    # Add buildinfo env variables
    buildinfo_env = os.environ.copy()
    if env:
        for k in env:
            buildinfo_env[k] = env[k]
    return buildinfo_env


class NBSR:

    def __init__(self, stream):
        """
        Non Blocking Stream Reader
        :param stream: the stream to read from.
                Usually a process' stdout or stderr.
        """

        self._s = stream
        self._q = Queue()

        def _populateQueue(stream, queue):
            """
            Collect lines from 'stream' and put them in 'queue'.
            """
            while True:
                line = stream.readline()
                if line:
                    queue.put(line)
                #else:
                #    raise UnexpectedEndOfStream

        self._t = Thread(target=_populateQueue, args=(self._s, self._q))
        self._t.daemon = True
        self._t.start()  # start collecting lines from the stream

    def readline(self):  # , timeout=None):
        """ Non-blocking readline
        """
        try:
            return self._q.get_nowait()
        except Empty:
            return None


class UnexpectedEndOfStream(Exception): pass

def run_command(command, env=None):
    """
    Runs a shell command

    :param command: string with the command to run
    :return: tuple with exit code and command output

    """
    log.debug('Running command: %s' % command)

    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True,
                         env=add_buildinfo_environment(env))
    out = ''
    while p.poll() is None:
        output = p.stdout.readline()
        output = output.strip()
        if output:
            log.info('   - %s', output)
            out += '%s\n' % output
            sys.stdout.flush()
    (output, err) = p.communicate()
    if output.strip():
        for line in output.split('\n'):
            line = line.strip()
            if line:
                log.info('   - %s', line)
                out += '%s\n' %line
    sys.stdout.flush()
    #if err.strip():
    #    for line in err.split('\n'):
    #        line = line.strip()
    #        if line:
    #            log.error('   - %s', line)
    #            out += '%s\n' %line
    #sys.stderr.flush()
    if p.returncode:
        raise Exception('- command terminated with error code: %s' % str(p.returncode))
    return 0, out

def run_command_nbsr(command, env=None):

    global output, errors
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, bufsize=1, close_fds=not settings.WIN32,
                         env = add_buildinfo_environment(env))
    nbsr_stdout = NBSR(p.stdout)
    nbsr_stderr = NBSR(p.stderr)
    while p.poll() is None:
        out = nbsr_stdout.readline()
        err = nbsr_stderr.readline()
        log_lines(out)
        log_lines(err, error=True)
    #sys.stdout.flush()
    #sys.stderr.flush()
    if p.returncode:
        print('[ERROR] - command "%s" terminated with error code: %s' % (command, str(p.returncode)))
        return 1, output, errors
    return 0, output, errors

def run_command_fcntl(command, env=None):
    p = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True,
                         env=add_buildinfo_environment(env))

    fd = p.stdout.fileno()
    fl = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)

    fd = p.stderr.fileno()
    fl = fcntl.fcntl(fd, fcntl.F_GETFL)
    fcntl.fcntl(fd, fcntl.F_SETFL, fl | os.O_NONBLOCK)

    out_buf, err_buf = '', ''
    while p.poll() is None:
        activity = False
        try:
            out_buf += p.stdout.read()
            log_lines(out_buf)
            activity = True
        except: pass
        try:
            err_buf += p.stderr.read()
            log_lines(err_buf, error=True)
            activity = True
        except: pass
        if not activity:
            time.sleep(0.01)
    (remaining_out, remaining_err) = p.communicate()
    log_lines(remaining_out)
    sys.stdout.flush()
    log_lines(remaining_err, error=True)
    sys.stderr.flush()
    if p.returncode:
        raise Exception('- command "%s" terminated with error code: %s' % (command, str(p.returncode)))
    return 0, output, errors