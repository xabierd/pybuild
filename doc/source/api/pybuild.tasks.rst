pybuild.tasks package
=====================

Subpackages
-----------

.. toctree::

    pybuild.tasks.ml

Submodules
----------

pybuild.tasks.build module
--------------------------

.. automodule:: pybuild.tasks.build
    :members:
    :undoc-members:
    :show-inheritance:

pybuild.tasks.prepare module
----------------------------

.. automodule:: pybuild.tasks.prepare
    :members:
    :undoc-members:
    :show-inheritance:

pybuild.tasks.promote module
----------------------------

.. automodule:: pybuild.tasks.promote
    :members:
    :undoc-members:
    :show-inheritance:

pybuild.tasks.publish module
----------------------------

.. automodule:: pybuild.tasks.publish
    :members:
    :undoc-members:
    :show-inheritance:

pybuild.tasks.releasenotes module
---------------------------------

.. automodule:: pybuild.tasks.releasenotes
    :members:
    :undoc-members:
    :show-inheritance:

pybuild.tasks.tag module
------------------------

.. automodule:: pybuild.tasks.tag
    :members:
    :undoc-members:
    :show-inheritance:

pybuild.tasks.task module
-------------------------

.. automodule:: pybuild.tasks.task
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: pybuild.tasks
    :members:
    :undoc-members:
    :show-inheritance:
