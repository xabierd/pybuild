
def default_parser(parser):
    """Parser settings for every action."""
    # Every action should have access to a proper log
    #log_parser(parser)
    # Every action can use  --pdb and --backtrace
    group = parser.add_argument_group("debug options")
    group.add_argument("--backtrace", action="store_true", help="Display backtrace on error")
    group.add_argument("--pdb", action="store_true", help="Use pdb on error")


def prepare_parser(parser):
    pass
