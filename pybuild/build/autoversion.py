"""
Obtains the build version form the current branch and existing project tags and release branches

"""
from ..common import initialize_logger, log_header, run_command, number_of_cores
log = initialize_logger(__name__)

import sys

from optiverversion import LongVersion, VersionList
from pybuild.conf import settings


class AutoVersion(object):

    def __init__(self, product, branch, tags, releases, type='release_branches'):
        """

        :param branch: Current branch
        :param tags: Existing tags
        :param releases: Existing release branches when current branch was created
        :param type: Strategy to define nex version.
        """
        self.product = product
        self.branch = branch
        self.tags = VersionList(tags, strict=False)
        self.releases = VersionList([self.clean_branch_name(r) for r in releases], strict=False)
        self.raw_releases = releases
        self.type = type

        if self.tags.invalid:
            log.warning('The following tags could not be parsed:\n%s' % '\n'.join(self.tags.invalid))
        if self.releases.invalid:
            log.warning('The following releases could not be parsed:\n%s' % '\n'.join(self.releases.invalid))

    def get_version(self, append_branch_name=True):
        """
        Returns the autoversion generated version for the rules defined by type

        :param append_branch_name: True if branch name will be appended as pre-release string
        :return: LongVersion object from optiver-version
        """
        log.info('Using auto-version\'s "%s" strategy.' % self.type)
        if self.type =='release_branches':
            return self.get_version_release_from_branches(append_branch_name=append_branch_name)
        elif self.type == 'trunk_minor':
            return self.get_version_release_from_main_minor(append_branch_name=append_branch_name)
        elif self.type == 'trunk_patch':
            return self.get_version_release_from_main_patch(append_branch_name=append_branch_name)
        else:
            log.error('Autoversion type "%s" is not implemented')
            sys.exit(-1)

    def next_patch_for_version(self, version):
        """
        Returns next patch for a given version by:
        * increment the patch for the latest patch for this version
        * if no patches are found, the next build version will be the patch .0

        :param version: LongVersion object from optiver-version
        """

        version_from = version
        version_to = version_from.next_minor("alpha")  # "alpha" version is the lowest version for a given minor
        log.debug('Getting tags between %s and %s' % (version_from, version_to))
        if not self.product in self.tags.versions:
            log.info('No tags for product "%s"' % self.product)
            return version_from
        patches = [t for t in self.tags.versions[self.product] if (version_from <= t < version_to)]
        if patches:
            log.debug('Patches for version %s: %s' % (version, [str(h) for h in patches]))
            last_tag = sorted(patches)[-1]
            next_version = last_tag.next_patch()
            return next_version
        else:
            log.debug('No patches for {ver}, will build version {ver}'.format(ver=version_from))
            return version_from

    @staticmethod
    def clean_branch_name(version):
        if version.endswith('.x'):
            version = version.replace('.x','.0')
        if version.endswith('_maintenance'):
            version = version.replace('_maintenance','')
        version = version.split('/')[-1]
        return version

    def _sanitize_suffix(self, suffix):
        sf = suffix
        replacements = [('/', '_'), ('.', '_')]
        for orig, repl in replacements:
            sf = sf.replace(orig, repl)
        return sf

    def get_version_release_from_branches(self, append_branch_name=True):
        """
        Tries to obtain the next build version for the current branch based on the existing SVN branches and tags.
        It assumes we are using release branches,

        :param append_branch_name: if true and not on a release branch, will append the current branch name as pre-release,
                ie: 1.2.3-trunk
        :return:
    """
        suffix = self._sanitize_suffix(self.branch)  # self.branch.replace('/', '_')
        suffix = settings.AUTOVERSION_PRERELEASE_PREFIX +  suffix if append_branch_name else ''

        # For release branches
        if self.branch in self.raw_releases:
            log.debug('%s is a release branch' % self.branch)
            match = settings.BRANCH_SIMPLE_REGEX.match(self.branch)
            version = self.clean_branch_name(self.branch)
            if match:
                # TODO: What if the release branch is full version: foo-1.2.3???
                new_version =self.next_patch_for_version(LongVersion(version, strict=False))
                log.info('Auto-version result: %s' % new_version)
                return new_version
            else:
                log.debug('Unable to identify version form branch %s' % self.branch)
                return None
        #For non release branches
        else:
            log.debug('%s is NOT a release branch' % self.branch)
            latest_release_branch = self.releases.last_version(self.product)
            if latest_release_branch:
                log.debug('Latest release branch: {version}'.format(version=latest_release_branch))
                next_minor = latest_release_branch.next_minor(suffix)
                log.info('Auto-version result: %s' % next_minor)
                return next_minor
            # If no release branches found, try to look into the tags
            else:
                log.debug('No release branches, trying to look into tags')
                last_tag = self.tags.last_version(self.product)
                if last_tag:
                    log.debug('Last tag is {last}'.format(last=last_tag))
                    next_minor = last_tag.next_minor(suffix)
                    log.info('Auto-version result: %s' % next_minor)
                    return next_minor
                else:
                    log.warning('Can\'t identify versions from: %s, using version 0.1.0' % self.tags)
                    return LongVersion.from_tuple((self.product, (0,0,0, ''))).next_minor(suffix)

    def get_version_release_from_main_minor(self, append_branch_name=True):

        suffix = settings.AUTOVERSION_PRERELEASE_PREFIX + self._sanitize_suffix(self.branch) if append_branch_name else ''
        # Release branch is trunk/master
        last_version = self.tags.last_version(self.product)
        if self.branch in ['trunk', 'master']:
            return last_version.next_minor() if last_version \
                else LongVersion.from_tuple((self.product, (0,0,0, ''))).next_minor()
        else:
            return last_version.next_minor(suffix) if last_version \
                            else LongVersion.from_tuple((self.product, (0,0,0, ''))).next_minor(suffix)

    def get_version_release_from_main_patch(self, append_branch_name=True):

        suffix = settings.AUTOVERSION_PRERELEASE_PREFIX + self._sanitize_suffix(self.branch) if append_branch_name else ''
        # Release branch is trunk/master
        last_version = self.tags.last_version(self.product)
        if self.branch in ['trunk', 'master']:
            return last_version.next_patch() if last_version \
                else LongVersion.from_tuple((self.product, (0,0,0, ''))).next_minor()
        else:
            return last_version.next_patch(suffix) if last_version \
                else LongVersion.from_tuple((self.product, (0,0,0, ''))).next_minor(suffix)
