from pybuild.common.common import *
log = initialize_logger(__name__)

import unittest

from pybuild.managers import LDAPOptiver


class TestLDAP(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.AD = LDAPOptiver()

    def test_get_user(self):
        self.assertEquals(['XabierDavila'], self.AD.get_user('xabierdavila')['mailNickname'])
        self.assertRaises(IndexError, self.AD.get_user, 'doesnotexist')