"""Runs preparation tasks
"""

from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)

from pybuild.conf import settings

from pybuild.tasks.task import Task
from pybuild.managers import Artifactory, BambooClient, ConfluenceClient, JIRAClient, Svn, Ftp, LDAPOptiver
from pybuild.build import Build

import os

class Prepare(Task):
    """Generic build preparation task
    """

    def __init__(self):
        Task.__init__(self)
        self.build = None

    @staticmethod
    def configure_parser(parser):
        """ Parser for generic build preparation task
        """
        Task.configure_parser(parser)
        # parser.add_argument_group("generic prepare options")
        parser.add_argument("--properties-file", help="File with build properties")
        parser.set_defaults(properties_file=settings.DEFAULT_PROJECT_PROPERTIES,
                            )

    def configure(self):
        Task.configure(self)

    def do(self, parser):

        Task.do(self, parser)

        self.build = Build(bamboo=BambooClient())
        self.build.init_vcs()

        self.build.parse_properties_file(self.args.properties_file)




