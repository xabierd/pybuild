"""Runs pre-build checks
"""

from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)

from pybuild.conf import settings
from pybuild.tasks.task import Task

import nose
import sys

class PreBuild(Task):
    """Generic pre-build checks
    """

    def __init__(self):
        Task.__init__(self)
        self.build = None

    @staticmethod
    def configure_parser(parser):
        """ Configure parser for this action
        """
        Task.configure_parser(parser)

    def configure(self):
        pass

    def do(self, parser):
        Task.do(self, parser)

        result = nose.run(argv=settings.DEFAULT_NOSE_ARGUMENTS, defaultTest= 'pybuild.checks:PreBuildChecks')
        if result:
            log.info(log_header('.') % 'Generic pre-build checks succeed')
        else:
            log.error(log_header('!') % 'Generic pre-build checks failed')
            sys.exit(-1)


