"""Pre-promotion checks
"""

from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)

from pybuild.tasks.task import Task
from pybuild.conf import settings

import nose
import sys

class PrePromote(Task):
    """Generic pre-promotion checks
    """

    def __init__(self):
        Task.__init__(self)
        self.build = None

    @staticmethod
    def configure_parser(parser):
        """ Configure parser for this action
        """
        Task.configure_parser(parser)

    def configure(self):
        log.info('Configuring task "%s"' % self.name)

    def do(self, parser):
        checks = ['pybuild.checks:PreBuildChecks', 'pybuild.checks:PrePromoteChecks']
        Task.do(self, parser)

        result = [nose.run(argv=settings.DEFAULT_NOSE_ARGUMENTS, defaultTest= c) for c in checks]
        if all(result):
            log.info(log_header('.') % 'Generic pre-promotion checks succeed')
        else:
            log.error(log_header('!') % 'Generic pre-promotion checks failed')
            sys.exit(-1)


