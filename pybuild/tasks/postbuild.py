"""Runs post-build checks
"""

from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)

from pybuild.tasks.task import Task
from pybuild.managers import Artifactory, BambooClient, ConfluenceClient, JIRAClient, Svn, Ftp, LDAPOptiver
from pybuild.build import Build

import nose
import sys

class PostBuild(Task):
    """Generic post-build checks
    """
    def __init__(self):
        Task.__init__(self)
        self.build = None

    @staticmethod
    def configure_parser(parser):
        """ Configure parser for this action
        """
        Task.configure_parser(parser)

    def configure(self):
        log.info('Configuring task "%s"' % self.name)

    def do(self, parser):
        Task.do(self, parser)
        checks = 'pybuild.checks:PostBuildChecks'
        argv = ['', '-v', '--with-xunit']
        result = nose.run(argv=argv, defaultTest=checks )
        if result:
            log.info(log_header('.') % 'Generic post-build checks succeed')
        else:
            log.error(log_header('!') % 'Generic post-build checks failed')
            sys.exit(-1)


