from tests import *
from pybuild.managers import StashClient


class TestStashClient(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.patcher_get = mock.patch('pybuild.common.restAPIClient.RestAPIClient.rest_get', fake_rest_get('stash'))
        cls.patcher_post = mock.patch('pybuild.common.restAPIClient.RestAPIClient.rest_post', fake_rest_post('stash'))
        cls.patcher_get.start()
        cls.patcher_post.start()
        cls.sc = StashClient(url='http://some.url', usr='foo', pwd='bar')

    def test_get_tags(self):
        cases = [
            ('PRJ', 'foo', [u'foo-0.1.0', u'foo-0.1.1', u'foo-0.1.2', u'foo-0.1.3']),
            ('PRJ', 'empty', [])
        ]

        for (prj, repo, tags) in cases:
            returned_tags = self.sc.get_tags(prj, repo)
            self.assertEqual(tags, returned_tags)

    def test_get_branches(self):
        cases = [
            ('PRJ', 'foo', [u'master', u'releases/foo-0.1.x', u'releases/foo-0.2.x']),
            ('PRJ', 'empty', [u'master'])
        ]

        for (prj, repo, branches) in cases:
            returned_branches = self.sc.get_branches(prj, repo)
            self.assertEqual(branches, returned_branches)

    def test_create_tag(self):
        self.sc.create_tag('PRJ', 'foo', 'foo-2.3.8','Testing tags', 'c3495c830b0acf54564b61aff62883b406fcb886')
        with self.assertRaises(HTTPError):
            self.sc.create_tag('NE', 'ne', 'tagname-1.2.3', 'msg', '34f4aad7b')
