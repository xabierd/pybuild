#!/bin/bash

## Build helper functions

# Add timestamp and other info to output messages
log(){
   case ${1} in
   	 INFO)
        echo "${1} : $(hostname) : ${0} : ${2}"
        ;;
     DEBUG)
   	    if [ "${LOGLEVEL}" == "DEBUG" ]; then
   	    	echo "${1} : $(hostname) : ${0} : ${2}"
   	    fi
   	    ;;
   	 ERROR)
		echo "${1} : $(hostname) : ${0} : ${2}" 1>&2
		exit 1
		;;
     HEADER)
        printf '#%.0s' {1..140} && printf '\n'
        printf ' %.0s' {1..20} && echo "$(hostname) : ${0} : ${2}"
        printf '#%.0s' {1..140} && printf '\n'
        ;;
   	 *)
		echo "${1} : $(hostname) : ${0} : ${2}" 1>&2
		;;
   esac
}