"""
Wrapper to get configuration either from environment (Bamboo builds)
or from arguments (manual build)
"""
from pybuild.common import initialize_logger, log_header, class_repr
log = initialize_logger(__name__)

from autoversion import AutoVersion

from pybuild.conf import settings

import os
import re
import sys
from getpass import getuser

from optiverversion import LongVersion
from pybuild.managers import Svn, Git

'''
Examples for content on build.reasonBuild report. Tested for below regex in pythex.org:
    Rebuilt     by <a href=\"http://bamboo.ams.optiver.com/browse/user/XabierDavila\">Xabier Davila</a>
    <span>Release build for <a href=\"http://jira.ams.optiver.com/browse/MLAMSOPM/fixforversion/32774\">ML OPM netpar_brokertec-1.0.1</a> by <a href=\"http://jira.ams.optiver.com/secure/ViewProfile.jspa?name=MaximilianMaldacker\">Maximilian Maldacker</a></span>
    Changes by <a href=\"http://bamboo.ams.optiver.com/browse/user/michelboersma\">Michel Boersma</a> and <a href=\"http://bamboo.ams.optiver.com/browse/user/radupopescu\">Radu Popescu</a>
    Custom build by <a href=\"http://bamboo.ams.optiver.com/browse/user/Catalinjora\">Catalin Jora</a>&nbsp;with revision <a href=\"http://bamboo.ams.optiver.com/browse/ML-IMCOV-1#changesSummary\">445088</a>
    <span>Release build for <a href="http://jira.ams.optiver.com/browse/DSTRDP/fixforversion/31627">Testing Releasing and Deployment Plans 0.6</a> by <a href="http://jira.ams.optiver.com/secure/ViewProfile.jspa?name=XabierDavila">Xabier Davila</a></span>
    Manual run by <a href="http://bamboo.ams.optiver.com/browse/user/XabierDavila">Xabier Davila</a>
    Manual run from the stage: <b>Promote</b> by <a href="http://bamboo.ams.optiver.com/browse/user/XabierDavila">Xabier Davila</a>
    Rebuilt     by <a href="http://bamboo.ams.optiver.com/browse/user/XabierDavila">Xabier Davila</a>
    Scheduled
    Code changes detected
    First build for this plan
'''

_REGEX_BUILD_USER = re.compile(r'''^(?:(?:<span>)*(?:Release|Manual|Changes|Rebuilt|Custom).*)(?:user/|name=)([a-zA-Z]+)(?:\\)*">(?:.*)$''')
_NOTJIRA = 'notjira'


class BuildConfig:
    """
    Build configuration
    """
    def __init__(self, config=None, bamboo=None, artifactory=None, vcs=None, jira=None, confluence=None):
        """

        :param config:
        :param bamboo: Bamboo manager object
        :param artifactory: Artifactory manager object
        :param vcs: VCS manager object
        :param jira: JIRA manager object
        :param confluence: Confluence manager object
        """

        self.productName     = None     #: product name
        self.binaryNames     = None     #: List of binary files produced by build. Used in rename and post-b checks. Defaults to product name if *bamboo_binary_names* is empty

        self.planKey         = None     #: Bamboo plan key, ie: ML-MLT3
        self.buildKey        = None     #: Bamboo build key, ie: ML-MLT3-55
        self.buildNumber     = None     #: Bamboo build number
        self.planName        = None     #: Bamboo plan name, ie: Marketlinks - Template

        #self.artifactoryCIRepo      = settings.DEFAULT_CI_REPO     #: Artifactory repository to use for CI build
        #self.artifactoryReleasesRepo = settings.DEFAULT_RELEASES_REPO  #: Artifactory repository to use for Releases
        self.artifactoryPath        = None     #: Artifactory path, picks the value from ``artifactory_path`` Bamboo plan variable
        self.vcs             = None     #: VCS manager object
        self.vcsURL          = None     #: VCS url
        self.vcsRevision     = None     #: VCS revision used in the build
        self.vcsRepoType     = None     #: VCS repo type can be either `svn` or `git`
        self.vcsRepoBranch   = None     #: VCS branch name

        self.autoVersionStrategy = None  #: What strategy to use for AutoVersion builds

        self.release_notes_extra_args = None  #: Additional args passed to RNs generator

        self.buildDir     = settings.DEFAULT_BUILD_DIR      #: directory to use for build
        self.artifactsDir = settings.DEFAULT_ARTIFACTS_DIR  #: directory where to place artifacts
        self.srcDir       = settings.DEFAULT_SRC_DIR        #: directory with the source checkout
        self.reportsDir   = settings.DEFAULT_REPORTS_FOLDER #: directory to store the produced reports

        self.confluence  = confluence    #: Confluence manager object
        self.bamboo      = bamboo        #: Bamboo manager object
        self.artifactory = artifactory   #: Artifactory manager object
        self.jira = jira if jira else log.warn("JIRA has not being initialized.") #: JIRA manager object

        self._jiraKey      = None

        self._buildVersion = None # Cache for buildVersion. Do not use directly, use get_build_version()
        self._buildUser    = None # Cache for buildUser. Do not use directly, use get_build_user()

        if bamboo:
            self.bamboo = bamboo        #: Bamboo manager object
            self.ciServerVersion = bamboo.get_server_version()
        else:
            log.warning("Bamboo configuration has not been initialized")
            self.ciServerVersion = "Manual build"

        self.artifactory = artifactory if artifactory else \
            log.warning("Artifactory configuration has not been initialized")

        #if config:
        #    self._init_manual_build(config)
        #else:
        self._init_bamboo_build()

        self.vcs = vcs if vcs else log.warn("VCS has not being initialized.")


    def _init_bamboo_build(self):
        """
        Get environment variables set by Bamboo

        """
        try:
            self.productName     = os.environ['bamboo_product_name']
            #self.buildTimeStamp  = os.environ['bamboo_buildTimeStamp']
            self.planKey         = os.environ['bamboo_planKey']   # ML-MLT3
            self.buildKey        = os.environ['bamboo_buildKey']  # ML-MLT3-RB
            self.buildKey        = os.environ['bamboo_buildKey']
            self.buildNumber     = os.environ['bamboo_buildNumber']
            self.planName        = os.environ['bamboo_planName']  # Team - PlanName
            self.buildName       = os.environ['bamboo_buildPlanName']  # Team - PlanName - JobName

            self.artifactoryCIRepo       = os.environ.get('bamboo_artifactory_ci_repo', settings.DEFAULT_CI_REPO)
            self.artifactoryReleasesRepo = os.environ.get('bamboo_artifactory_releases_repo', settings.DEFAULT_RELEASES_REPO)
            self.artifactoryPath         = os.environ['bamboo_artifactory_path']

            self.vcsURL          = os.environ['bamboo_planRepository_repositoryUrl'].rstrip('/')
            self.vcsRevision     = os.environ['bamboo_planRepository_revision']
            self.vcsRepoType     = os.environ['bamboo_planRepository_type']
            self.vcsRepoBranch   = os.environ['bamboo_planRepository_branchName']

            self.autoVersionStrategy = os.environ.get('bamboo_pybuild_autoversion_strategy', 'release_branches')

            self.release_notes_extra_args = os.environ.get('bamboo_release_notes_extra_args')

            self.binaryNames              = os.environ.get('bamboo_binary_names','').split()
            if not self.binaryNames:
                self.binaryNames = self.productName.split()

            # Optional variables, may not be defined
            self.isLib            = os.environ.get('bamboo_library_build', 'FALSE') == 'TRUE'
            self.bamboo_buildUser = os.environ.get('bamboo_ManualBuildTriggerReason_userName')
            self.bamboo_jiraUser  = os.environ.get('bamboo_jira_username')

        except Exception, e:
            log.error('Error initializing Bamboo build: %s' % e.message)
            sys.exit(-1)

    @property
    def repo_type(self):
        """
        Returns repo type
        :return: string with either `git` or `svn`
        """
        if 'svn' in self.vcsRepoType:
            return 'svn'
        elif any([s in self.vcsRepoType for s in ('git', 'stash')]):
            return "git"
        else:
            log.error("Repository type can not be determined from '%s'" % self.vcsRepoType)
            sys.exit(-1)

    def init_vcs(self, vcs=None, product_name=None):
        """
        Initialize the VCS settings, either by initializing the proper VCS object or
        using the provided object

        :param vcs: if a VCS abject is provided, use this

        """
        if vcs:  # VCS provided
            self.vcs = vcs
        else:
            log.info('Initializing VCS')
            if self.repo_type == 'svn':
                log.info('Initializing subversion repository')
                self.vcs = Svn(url_or_path=self.vcsURL, revision=self.vcsRevision)
            elif self.repo_type == 'git':
                log.info('Initializing git repository')
                self.vcs = Git(settings.DEFAULT_SRC_DIR, url=self.vcsURL, revision=self.vcsRevision)
            else:
                log.error('Unknown VCS type: %s' % self.repo_type)
                sys.exit(-1)

    @property
    def short_revision(self):
        try:
            int(self.vcsRevision)
            return self.vcsRevision
        except ValueError:
            return self.vcsRevision[0:7]

    def parse_properties_file(self, props_file):
        # Export variables
        properties = {}

        # Try to get project.properties file
        props_file = os.path.join(self.srcDir, props_file)

        # If there is a project properties file, read it so the props are added to the generated properties
        if os.path.isfile(props_file):
            with open(props_file,'r') as fl:
                try:
                    for line in fl:
                        key, value = line.split('=')
                        properties[key] = value.strip()
                except Exception, e:
                    log.error('Error reading %s: %s' % (props_file, e.message))

        # Add calculated version, if not in properties
        if not 'version' in properties:
            properties['version'] = self.get_build_version()
            log.info(log_header('/') % 'This branch will build version %s' % properties['version'])
        else:
            properties['version'] = self._buildVersion = properties['version']

        # Add revision and short_revision
        properties['revision'] = self.vcsRevision
        properties['short_revision'] = self.short_revision

        # Export to generated properties file
        with open('generated_project.properties', 'w+') as fl:
            for key, value in properties.items():
                fl.write('{key}={value}\n'.format(key=key, value=value))


    def get_build_version(self, inject=None):
        """
        Logic to choose the current product version.

        Priority is:

        1. `bamboo_jira_version`
        2. `bamboo_inject_version`
        3. extract from VCS releases/tags

        :return: short version string: 1.2.3, 0.2-beta.1

        """
        if self._buildVersion:
            return self._buildVersion
        bamboo_jira_version = os.environ.get('bamboo_jira_version')
        if bamboo_jira_version:
            # Use manually/JIRA provided version. Validated via the LongVersion class
            try:
                self._buildVersion = LongVersion(bamboo_jira_version).version
            except ValueError:
                self._buildVersion = LongVersion('-'.join([self.productName, bamboo_jira_version])).version
        elif os.environ.get('bamboo_inject_version'):
            # Use project.properties to get version
            self._buildVersion = LongVersion(self.productName + '-' + os.environ.get('bamboo_inject_version')).version
        elif inject:
            # Need this to give priority to jira_version
            # TODO: Change this to something more clear
            self._buildVersion = inject
        elif self.vcs:
            # Extract version from releases/tags
            tags = self.vcs.get_tags()
            if self.repo_type == 'svn':
                releases = self.vcs.get_release_branches(rev=self.vcs.get_branch_creation_revision(self.vcsURL),
                                                         product_name=self.productName)
            elif self.repo_type == 'git':
                releases = self.vcs.get_release_branches()
            else:
                log.error('Unknown repo type')
                sys.exit(-1)
            av = AutoVersion(self.productName, self.vcsRepoBranch, tags, releases, type=self.autoVersionStrategy)
            self._buildVersion = av.get_version().version
        else:
            log.error(log_header() % 'Version not supplied. In order to get it from VCS need to initialize a VCS object')
            sys.exit(-1)
        return self._buildVersion

    def get_build_user(self):
        """
        Returns the user that has triggered the build.

        It will return a legitimate user if the build stage has been triggered either manually or via
        JIRA release. Otherwise it will default to the current user, that is `bamboo` for Bamboo run builds

        :return: string with the username in lowercase

        """
        # Try getting from cache
        if self._buildUser:
            return self._buildUser
        # Manual build triggered from Bamboo
        if self.bamboo_buildUser:
            log.debug('Build user picked from Bamboo manual trigger: %s' % self.bamboo_buildUser)
            self._buildUser = self.bamboo_buildUser.lower()
            return self._buildUser
        # Build triggered from JIRA
        if self.bamboo_jiraUser:
            log.debug('Build user picked from JIRA release: %s' % self.bamboo_jiraUser)
            self._buildUser = self.bamboo_jiraUser.lower()
            return self._buildUser
        # User not provided by Bamboo, will try other options
        if self.bamboo:
            br = self.bamboo.get_build_report(self.buildKey, self.buildNumber)
            match = _REGEX_BUILD_USER.match(br['reasonSummary'])
            if match:
                self._buildUser = match.group(1).lower()
                return self._buildUser
            log.warning('Can\'t extract user from Bamboo info: %s' % br['reasonSummary'])
        log.warning('Unable to identify the build user. Defaulting to current user: %s' % getuser())
        self._buildUser = getuser().lower()
        return self._buildUser

    @property
    def build_metadata(self):
        return '+b%s.%s' % (self.buildNumber, self.vcsRevision)

    @property
    def full_version_tag(self):
        """
        Returns the full product name, that is ``product``-``version``
        """
        return '-'.join([self.productName, self.get_build_version()])

    @property
    def full_version_dev(self):
        """
        Returns the full product name, that is ``product``-``version``
        """

        return '-'.join([self.productName, self.get_build_version()]) + self.build_metadata

    @property
    def jira_key(self):
        if not self._jiraKey:
            if os.environ.get('bamboo_jira_key'):
                self._jiraKey = os.environ.get('bamboo_jira_key')
            elif self.repo_type == 'svn':
                self._jiraKey = self.vcs.get_jira_project()
            else:
                log.error('Can\'t obtain JIRA project key')
                sys.exit(-1)
        return self._jiraKey

    def get_full_binary_names(self):
        """
        Returns a list with the full binary name, that is ``['foo-1.2.3', 'bar-1.2.3']``
        """
        return [ '%s-%s' % (bname, self.get_build_version()) for bname in self.binaryNames]

    def get_artifactoryProductPath(self):
        """
        Returns the full Artifactory path for the current product and version. That is
        ``base artifactory path``/``product name``/``version``
        """
        return '/'.join([self.artifactoryPath, self.productName, self.get_build_version()])

    def __repr__(self):
        return class_repr(vars(self))
