"""
Set of random functions and variables used in different places of ``pyBuild``

"""

import logging
import sys
import os
import re
import json
import platform
import zipfile
import shutil
import errno
from requests_oauthlib import OAuth1

from pybuild.conf import settings



if platform.system() == "Windows":
    import pyreadline as readline
else:
    import readline


def log_header(char='=', length = 140):
    return '\n'.join(['\n' + char*length, '\t'*4 + '%s', char*length])

class LogFilter(logging.Filter):
    """
    Used by :func:`initialize_logger` to redirect errors to ``stderr``
    """
    def filter(self, rec):
        return rec.levelno in (logging.DEBUG, logging.INFO, logging.WARNING)


def initialize_logger(module_name):
    """
    Log initialization

    :param module_name: module name
    :return: logger object
    """

    levels = {'DEBUG': logging.DEBUG,
              'INFO': logging.INFO,
              'WARNING': logging.WARNING,
              'ERROR': logging.ERROR}
    log_level = levels[os.environ.get('bamboo_loglevel', 'DEBUG')]
    logger = logging.getLogger(module_name)
    #logging.addLevelName( logging.WARNING, "\033[1;33m%s\033[1;0m" % logging.getLevelName(logging.WARNING))
    #logging.addLevelName( logging.ERROR, "\033[1;31m%s\033[1;0m" % logging.getLevelName(logging.ERROR))
    #logging.addLevelName( logging.DEBUG, "\033[1;34m%s\033[1;0m" % logging.getLevelName(logging.DEBUG))
    # Set log format
    if log_level == logging.DEBUG:
        log_fmt_string = '{host}: %(name)s: [%(levelname)-7s] - %(message)s'
    else:
        log_fmt_string = '{host}: [%(levelname)-7s] - %(message)s'
    formatter = logging.Formatter(log_fmt_string.format(host=settings.HOSTNAME))

    stdoutHandler = logging.StreamHandler(sys.stdout)
    stdoutHandler.setLevel(logging.DEBUG)
    stdoutHandler.addFilter(LogFilter())
    stdoutHandler.setFormatter(formatter)
    stderrHandler = logging.StreamHandler(sys.stderr)
    stderrHandler.setLevel(logging.ERROR)
    stderrHandler.setFormatter(formatter)
    logger.propagate = 0
    logger.addHandler(stdoutHandler)
    logger.addHandler(stderrHandler)
    logger.setLevel(log_level)
    return logger

log = initialize_logger(__name__)
log.setLevel(logging.DEBUG)


def class_repr(attributes):
    """
    Used in different classes special methods ``__repr__`` to print the class attributes.
    It will filter any attributes starting with `passw`, `auth` and `_`

    :param attributes: list of attributes
    :return: string with a line `attribute: value` per input attribute

    """
    return '\n'.join("\t%s: %s" % item for item in sorted(attributes.items())
                            if not (item[0].startswith('passw') or
                                    item[0].startswith('auth') or
                                    item[0].startswith('_')
                                    )
                    )


def get_auth(app):
    """
    Returns the `auth` tuple corresponding to the provided `app`. This can be either (user,pass) or,
    for apps that support OAuth, the corresponding  OAuth tuple.

    :param app: string with application to get auth for
    :return: `auth` tuple as understood by :class:`restAPIClient`
    """

    data = json.loads(open(settings.CREDS_FILE).read())
    if app in ['bamboo', 'jira', 'confluence', 'stash']:
        key_cert_data = open(settings.CREDS_KEY_FILE, 'r').read()
        app_key = 'BuildScripts'
        app_secret = ''
        user_oauth_token = data[app]['user_oauth_token']
        user_oauth_token_secret = data[app]['user_oauth_token_secret']
        auth = OAuth1(app_key,
                      app_secret,
                      user_oauth_token,
                      user_oauth_token_secret,
                      signature_method='RSA-SHA1',
                      rsa_key=key_cert_data)
    elif app in ['artifactory', 'ldap']:
        usr = data[app]['user']
        pwd = data[app]['pass']
        auth = (usr, pwd)
    else:
        log.error('Can\'t get Auth for app: %s' % app)
        sys.exit(-1)
    return auth


def url_exists(url):
    """
    Checks if provided URL exists, that is, no errors when trying to access it
    """
    import urllib2
    try:
         urllib2.urlopen(url)
    except urllib2.HTTPError, e:
         print(e.code)
         return False
    except urllib2.URLError, e:
         print(e.args)
         return False
    return True

def number_of_cores():
    """
    Returns number of cores in the system processor
    """
    import multiprocessing
    return multiprocessing.cpu_count()

def multiple_replace(replacements, text):
  """
  Performs a multiple replace in the provided text, as indicated by the dictionary data.
  ie: if our dictionary is::

      {
        'foo' : 'bar,
        'one' : 'two',
      }

  It will replace every occurrence od `foo` for `bar` and every occurrence of `one` for `two`

  :param replacements: dictionary with replacements
  :param text: string to make the replacements into
  :return: the new string with the replacements done

  """
  # Create a regular expression  from the dictionary keys
  regex = re.compile("(%s)" % "|".join(map(re.escape, replacements.keys())))
  # For each match, look-up corresponding value in dictionary
  return regex.sub(lambda mo: replacements[mo.string[mo.start():mo.end()]], text)


def get_input(prompt, default):
    """
    Reads a user input from the terminal
    :param prompt: string to print before prompting
    :param default: default response value
    :return: string with user input
    """
    readline.set_startup_hook(lambda: readline.insert_text(default))
    try:
        return raw_input(prompt + ': ')
    finally:
        readline.set_startup_hook()


def unzip(source_filename, dest_dir):
    """
    From here:
    http://stackoverflow.com/questions/12886768/how-to-unzip-file-in-python-on-all-oses

    :param source_filename:
    :param dest_dir:
    :return:
    """
    with zipfile.ZipFile(source_filename) as zf:
        for member in zf.infolist():
            # Path traversal defense copied from
            # http://hg.python.org/cpython/file/tip/Lib/http/server.py#l789
            words = member.filename.split('/')
            path = dest_dir
            for word in words[:-1]:
                drive, word = os.path.splitdrive(word)
                head, word = os.path.split(word)
                if word in (os.curdir, os.pardir, ''): continue
                path = os.path.join(path, word)
            zf.extract(member, path)

def copy(src, dest):
    try:
        shutil.copytree(src, dest)
    except OSError as e:
        # If the error was caused because the source wasn't a directory
        if e.errno == errno.ENOTDIR:
            shutil.copy(src, dest)
        else:
            log.error('Directory not copied. Error: %s' % e)

def clean_folders(f_list, create=True):
    """
    Deletes and re-creates the folders passed as a list.
    Folder paths are relative
    :param f_list: List of folders to clean
    """
    for folder in f_list:
        if os.path.isdir(folder):
            shutil.rmtree(folder)
            if create: os.mkdir(folder)

