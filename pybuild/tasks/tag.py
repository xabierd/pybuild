

from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)

from pybuild.conf import settings

from pybuild.tasks.task import Task
from pybuild.managers import Artifactory, BambooClient, ConfluenceClient, JIRAClient, Svn, Ftp, LDAPOptiver
from pybuild.build import Build

import os
import sys

class Tag(Task):
    """Generic create a tag
    """
    def __init__(self):
        Task.__init__(self)
        self.build = None

    @staticmethod
    def configure_parser(parser):
        """ Parser for generic build preparation task
        """
        Task.configure_parser(parser)
        parser.add_argument('-t', '--tag-name', help='Tag name')
        parser.add_argument('-c', '--tag-message', help='Comment in release history')
        parser.add_argument('--pin-svn-externals', action='store_true', help='Pin externals before creating tag')

    def configure(self):
        Task.configure(self)
        self.build = Build(bamboo=BambooClient())
        self.build.init_vcs()


    def do(self, parser):
        Task.do(self, parser)
        tag_name    = self.build.full_version_tag if self.args.tag_name is None else self.args.tag_name
        tag_message = settings.DEFAULT_TAG_LOG_MESSAGE.format(longversion=tag_name) if self.args.tag_message is None else self.args.tag_message

        if self.args.pin_svn_externals:
            if self.build.repo_type == 'svn':
                svn = Svn(settings.DEFAULT_SRC_DIR)
                externals_file = os.path.join(settings.DEFAULT_ARTIFACTS_DIR, settings.PINNED_EXTERNALS)
                try:
                    with open(externals_file, 'r') as fl:
                        externals = fl.read()
                except IOError, e:
                    log.error('Can\'t open externals file: %s' % e.message)
                    sys.exit(-1)
                svn.set_property('svn:externals', externals)
                log.info('Externals from "%s" set for working copy at "%s"' % (externals_file, settings.DEFAULT_SRC_DIR))
                svn.create_tag(tag_name, tag_message)
            else:
                log.error('Pin externals can only be done with SVN repositories')
        else:
            self.build.vcs.create_tag(tag_name, message=tag_message, revision=self.build.vcs.revision)
