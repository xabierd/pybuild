from ..common import initialize_logger, RestAPIClient
log = initialize_logger(__name__)

from ..conf import settings

import simplejson as json


class JIRAClient(RestAPIClient):
    def __init__(self, url=settings.JIRA_URL, auth=None,
                 projectKey=None):
        if not auth:
            auth = self.get_auth('jira')
        RestAPIClient.__init__(self, url, auth=auth)

        self.projectKey = projectKey
        #TODO: Implement cache self.cache = {}

        self._api = '/rest/api/latest'
        self._project = self._api + '/project/{projectKey}'
        self._info = self._api + '/serverInfo'
        self._jql = self._api + 'search?jql={query}'

    def get_project_by_key(self, pKey):
        log.debug('Invoking "get_project_by_key"')
        projectKey = pKey if pKey else self.projectKey
        url = self._project.format(projectKey=projectKey)
        resp = self.rest_get(url)
        if resp.status_code != 200:
            log.error("Error retrieving project: {} {}".format(resp.status_code, resp.text))
            return None
        project_info = json.loads(resp.text)
        # log.debug('Project info: %s' % project_info)
        return project_info


    def get_versions(self, projectKey=None):
        prj = self.get_project_by_key(projectKey)
        if prj:
            return prj['versions']
        return list()

    def get_version_details(self, version_name, projectKey=None):

        project_list = []

        if projectKey:
            if not isinstance(projectKey, list):
                project_list = [projectKey]
        else:
            project_list = self.projectKey

        for prj in project_list:
            versions = self.get_versions(prj)
            for v in versions:
                if v['name'] == version_name:
                    return  v
        return None

    def get_issues_from_jql(self, query, limit=10000):
        log.debug('Running query:"%s"' % query)
        url = self._jql.format(query=query) + '&maxResults={limit}'.format(limit=limit)
        resp = self.rest_get(url)
        if resp.status_code != 200:
            log.error("Error retrieving JQL query: {} {}".format(resp.status_code, resp.text))
            return None
        results = json.loads(resp.text)
        log.debug('Query results: %s' % results)
        return results

    def version_exists(self, version):
        jira_versions = self.get_versions()
        if any(v['name'] == version for v in jira_versions):
            return True
        log.debug('Version %s does not exist in JIRA project: %s' % (version, self.projectKey))
        return False

    def version_released(self, version):
        jira_versions = self.get_versions()
        if any(v['released'] == True for v in jira_versions if v['name'] == version):
            log.debug('Version %s has already been released in JIRA' % version)
            return True
        return False

    @staticmethod
    def GetIssueStatusName(issue):
        return issue['fields']['status']['name']

    @staticmethod
    def GetIssuePriorityName(issue):
        return issue['priority']['name']

    @staticmethod
    def GetIssueResolutionName(issue):
        return issue['resolution']['name']

    @staticmethod
    def GetIssueTypeName(issue):
        return issue['type']['name']

