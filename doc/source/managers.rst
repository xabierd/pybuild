Managers:
---------

.. toctree::
   :maxdepth: 2

   managers/artifactory
   managers/bamboo
   managers/jira
   managers/subversion