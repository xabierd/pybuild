"""ML pre-bu8ild checks
"""

from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)

from pybuild.conf import settings
from pybuild.tasks.prebuild import PreBuild

import nose
import sys

class PrebuildML(PreBuild):
    """Marketlinks pre-build checks
    """

    def __init__(self):
        PreBuild.__init__(self)

    @staticmethod
    def configure_parser(parser):
        """ Configure parser for this action
        """
        PreBuild.configure_parser(parser)
        parser.add_argument('--no-eml-check', action='store_true', help='Do not run EML check')

    def configure(self):
        PreBuild.configure(self)

    def do(self, parser):
        # PreBuild.do(self, parser)
        log.info(log_header() % 'Running "%s" task...' % self.name)
        self.parser = parser
        self.args = self.parser.parse_args()

        checks = ['pybuild.checks.ml:PreBuildChecks']
        if not self.args.no_eml_check:
            checks.append('pybuild.checks.ml:EMLVersionCheck')

        result = nose.run(argv=settings.DEFAULT_NOSE_ARGUMENTS, defaultTest=checks )
        if result:
            log.info(log_header('.') % 'Pre-build checks succeed')
        else:
            log.error(log_header('!') % 'Pre-build checks failed')
            sys.exit(-1)


