from tests import *

class TestConfluenceClient(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.conf = ConfluenceClient(url='http://idontcare.com', usr='dummy', pwd='pass0')

    @mock.patch('pybuild.common.RestAPIClient.rest_get')
    def test_get_content(self, mock_get):
        cases = [{'spaceKey': None, 'title': None, 'page_id': '777666222',
                  'response': '{"body": {"storage": {"value": "Original"}}}'},
                 {'spaceKey': 'DK', 'title': 'Some page', 'page_id': None,
                  'response': '{"results": [{"body": {"storage": {"value": "Original"}}}]}'},
                 ]
        mock_get.return_value = Response()
        mock_get.return_value.status_code = 200
        for c in cases:
            mock_get.return_value._content = c['response']
            mock_get.return_value.status_code = 200
            self.assertIn('body', self.conf.get_content(page_id=c['page_id'], space_key=c['spaceKey'], title=c['title']))
            mock_get.return_value.status_code = 404
            self.assertFalse(self.conf.get_content(page_id=c['page_id'], space_key=c['spaceKey'], title=c['title']))

    @mock.patch('pybuild.common.RestAPIClient.rest_post')
    def test_create_page(self, mock_post):
        cases = [{'content': 'Test content', 'spaceKey': 'DUMMY', 'title': 'Dummy title', 'parid': 56565, 'result': 56565}]
        for c in cases:
            mock_post.return_value = Response()
            mock_post.return_value.status_code = 200
            mock_post.return_value._content = '{"id": "9898989"}'
            resp = self.conf.create_page(space_key=c['spaceKey'], parent_id=c['parid'],
                                         title=c['title'], content=c['content'])
            mock_post.assert_called()
            self.assertEquals(resp, '9898989')

    @mock.patch('pybuild.managers.ConfluenceClient.create_page')
    def test_create_page_tree(self, mocked_create_page):
        cases = [{'spaceKey': 'DUMMY', 'parent_title': 'Testpage', 'tree': ['one','two','three']}]
        for c in cases:
            mocked_create_page.return_value = 12332144
            self.assertTrue(self.conf.create_page(c['spaceKey'], c['parent_title'], c['tree']))
            mocked_create_page.assert_called()

    #@mock.patch('common.RestAPIClient.rest_get')
    #@mock.patch('managers.ConfluenceClient.get_content')
    #@mock.patch('common.RestAPIClient.rest_put')
    #def test_modify_page(self, mock_put, mock_get_content, mock_get):
    #    cases = [{'spaceKey': 'DUMMY', 'title': 'Dummy title', 'content': 'Adding content', 'append': True},
    #             {'spaceKey': 'DUMMY', 'title': 'Dummy title', 'content': 'Replacing content', 'append': False}]
    #    mock_put.return_value = Response()
    #    mock_put.return_value.status_code = 200
    #    mock_get_content.return_value = {'id': '888888'}
    #    mock_get.return_value = Response()
    #    mock_get.return_value.status_code = 200
    #    mock_get.return_value._content = '{"body": {"storage": {"value": "Original"}}, "version": {"number": 10}}'
    #    for c in cases:
    #        self.assertTrue(self.conf.modify_page(c['spaceKey'], c['title'], c['content'], c['append']))

    #def test_adHoc(self):
    #    conf = ConfluenceClient('http://devwiki-test.ams.optiver.com', usr='hudson',pwd='Hud50N001')
    #    conf.modify_page('RN','bla', '<p>Adding more new content</p>')
    #    #conf.create_page_tree('RN', 'Testpage', 'Testpage/Team/product/product-1.2.3'.split('/'),
    #    #                      content='<p>Adding even more content on creation</p>')