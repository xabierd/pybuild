from ..common import initialize_logger, class_repr, log_header
log = initialize_logger(__name__)

from ..conf import settings

# pip install http://pysvn.barrys-emacs.org/source_kits/pysvn-1.7.8.tar.gz
import pysvn
import urlparse
import posixpath

from datetime import datetime

from optiverversion import LongVersion

import sys
import os
import re


# Needed for pySvn
def get_login(realm, username, may_save):
    """
    Since we expect to used the cached credentials, this should never be called
    """
    log.error("No SVN credentials cached")
    sys.exit(-1)


def get_log_message():
    return True, svn_commit_message

svn_commit_message = ''


def pysvn_revision(rev):
    """
    Returns a pysvn.Revision object corresponding to the provided version

    :param rev: int or pysvn.Revision object
    :return: pysvn.Revision
    """
    if rev:
        if type(rev) is int:
            return pysvn.Revision(pysvn.opt_revision_kind.number, rev)
        else:
            return rev
    else:
        return pysvn.Revision(pysvn.opt_revision_kind.unspecified)

def externals_to_svn17(externals):
    """
    Given an externals dictionary list, returns the externals string formatted for svn 1.7
    The dictionary list format is::

        [
            {'folder': 'x_common_libs',
             'url' : 'http://...'
             'peg_rev': 345,
             'op_rev': 345
            },
            ...
        ]
    """
    externals_string = ''
    for ex in externals:
        if ex.get('op_rev'):
            externals_string += '-r%s\t' % ex['op_rev']
        externals_string += '\t\t%s' % ex['url']
        if ex.get('peg_rev'):
            externals_string += '@%s\t' % ex['peg_rev']
        externals_string += '\t%s\n' % ex['folder']

    return externals_string

class Svn():
    """
    SVN Wrapper class
    """
    depth_empty = pysvn.depth.empty

    def __init__(self, url_or_path, revision=None, user=None, password=None):

        # Init svn client
        self.client = pysvn.Client()
        self.client.callback_get_login = get_login
        self.client.callback_get_log_message = get_log_message
        self.client.set_auth_cache(False)  # Not caching creds, if supplied

        self.url_or_path = url_or_path.rstrip('/')
        self.url = None
        self.revision = revision
        self.working_copy = None

        if os.path.isdir(url_or_path):
            self._init_local_svn()
        else:
            self._init_remote_svn(revision=revision)


    def __repr__(self):
        return class_repr(vars(self))

    def _init_remote_svn(self, revision=None):
        self.url = self.url_or_path
        if revision:
            self.revision = pysvn.Revision(pysvn.opt_revision_kind.number, revision)
        else:
            self.revision = pysvn.Revision(pysvn.opt_revision_kind.head)

    def _init_local_svn(self):
        self.working_copy = self.url_or_path
        info = self.client.info(self.url_or_path)
        self.url = info.data['url']
        self.revision = info.data['revision']

    def _url_exists(self, url):
        try:
            self.client.proplist(url)
        except pysvn.ClientError, e:
            return False
        return True

    def get_property(self, prop, recurse=False):
        """
        Returns string with contents of svn property

        :param prop: property name
        :param recurse: return properties recursively
        :return: string with property values
        """
        if self.url_or_path == self.working_copy:
            props = self.client.propget(prop, self.url_or_path, recurse=recurse)
        else:
            props = self.client.propget(prop, self.url_or_path, self.revision, recurse=recurse)
        log.debug(props)
        if props:
            return props[self.url_or_path]
        return ''

    def set_property(self, prop, value):
        """
        Sets a svn property for svn working copy

        :param prop: property to set
        :param value: value of the property
        """
        if self.working_copy:
            return self.client.propset(prop, value, self.working_copy)
        log.error('Svn object has no working copy associated\n %s' % self.__repr__())
        sys.exit(-1)

    @property
    def base_url(self):
        """
        Returns the project base url, based on the supplied schema.
        For example, for ``http://svn.foo/team/project/branches/somebranch`` it will return
        ``http://svn.foo/team/project`` bases on a standards SVN schema (trunk, branches, tags, releases)
        If none of the schema folders is found, returns the url as base url
        :return: project URL
        """
        for folder in settings.SVN_SCHEMA:
            if folder in self.url:
                return self.url.split('/%s' % folder)[0]
        return self.url

    @property
    def tags_url(self):
        url = '/'.join([self.base_url, 'tags'])
        if self._url_exists(url):
            return url
        return None

    @property
    def branches_url(self):
        url = '/'.join([self.base_url, 'branches'])
        if self._url_exists(url):
            return url
        return None

    @property
    def releases_url(self):
        url = '/'.join([self.base_url, 'releases'])
        if self._url_exists(url):
            return url
        return None

    def list(self, url, rev=None, peg_rev=None):
        """ Plain list with the contents of the folder
        """
        revision = pysvn_revision(rev)
        peg_revision = pysvn_revision(peg_rev)
        list_items = self.client.list(url, revision=revision, peg_revision=peg_revision)
        return [posixpath.basename(urlparse.urlparse(elem[0].get('path'))[2])
                for elem in list_items[1:]]

    def tag_exists(self, tag_name):
        return self._url_exists('/'.join([self.tags_url, tag_name]))

    def get_release_branches(self, rev=None, product_name=None):
        """
        Returns a list with the release branches for the svn object

        :param rev: revision when to check for release branches
        :return: list of strings with release branches names
        """
        if self.releases_url:
            if product_name:
                return [ br for br in self.list(self.releases_url, peg_rev=rev)
                         if br.startswith(product_name + '-')]
            else:
                log.warn('No product name supplied, returning all release branches')
                return self.list(self.releases_url, rev=rev)
        else:
            if product_name:
                log.warning('No "releases" url, defaulting to: %s-*' % product_name)
                return [release
                    for release in self.list(self.branches_url, rev=rev)
                    if release.startswith(product_name + '-')]
            else:
                log.error('No product name or releases url, can\'t get release branches')
                sys.exit(-1)

    def get_tags(self, rev=pysvn.Revision(pysvn.opt_revision_kind.head)):
        """
        Returns existing tags at a given revision (default is HEAD)

        :param rev: int or pysvn.Revision
        :return: list of tags
        """
        if self.tags_url:
            return self.list(self.tags_url, rev=rev)
        return None

    def is_release_branch(self, branch_name, rev=None):
        """
        Checks if a given branch is a release branch

        :param branch_name: Branch name
        :param rev: Revision, defaults to HEAD
        :return: True/False
        """
        release_branches = self.get_release_branches(rev=rev)
        if release_branches:
            return branch_name in release_branches
        return False

    def get_branch_creation_revision(self, url):
        """
        For "main" branch it will return the latest revision.
        For other branches will return the branch creation revision

        :param url: Full URL to the branch
        :return: 'pysvn.Revision' object
        """
        if '/trunk' in url:
            return pysvn.Revision(pysvn.opt_revision_kind.head)
        else:
            rev = pysvn.Revision(pysvn.opt_revision_kind.number,
                                  self.client.log(url)[-1].get('revision').number)
            log.debug('Branch %s created in revision %r' % (url, rev))
            return rev

    def copy(self, src_url_or_path, dest_url_or_path, revision=None, message=None):
        global svn_commit_message
        if message is None:
            svn_commit_message = "Copying from {orig} to {dest}".format(orig=src_url_or_path, dest=dest_url_or_path)
        else:
            svn_commit_message = message
        try:
            if revision:
                self.client.copy(src_url_or_path, dest_url_or_path, pysvn_revision(revision))
            else:
                self.client.copy(src_url_or_path, dest_url_or_path)
        except pysvn.ClientError, e:
            log.error("Error copying from {orig} to {dest}".format(orig=src_url_or_path, dest=dest_url_or_path))
            log.error(e.message)
            sys.exit(-1)
        log.info('Successfully copied from "{orig}" to "{dest}"'.format(orig=src_url_or_path, dest=dest_url_or_path))

    def checkout(self, folder, revision=None, depth=pysvn.depth.infinity):
        if revision:
            rev = pysvn_revision(revision)
        else:
            rev = pysvn.Revision(pysvn.opt_revision_kind.head)

        self.working_copy = os.path.abspath(folder)
        return self.client.checkout(self.url, folder, revision=rev, depth=depth)

    def commit(self, msg=None):
        if self.working_copy and msg:
            return self.client.checkin([self.working_copy], msg)
        log.error('Working copy or msg not provided')
        sys.exit(-1)

    def create_tag(self, tag_name, message=None, revision=None):
        if message is None:
            message = settings.TAG_COMMIT_MSG.format(tag_name=tag_name)
        if self.tag_exists(tag_name):
            log.error("The tag '{tag_name}' already exists in {tags_url}".
                      format(tag_name=tag_name, tags_url=self.tags_url))
            sys.exit(-1)
        else:
            new_tag_url = '/'.join([self.tags_url, tag_name])
            self.copy(self.url_or_path, new_tag_url, revision=revision, message=message)
            log.info("Created tag {tag_name} at {tag_url}".format(tag_name=tag_name, tag_url=new_tag_url))

    def get_info(self):
        raw = self.client.info2(self.url, recurse=False, revision = self.revision)
        data = raw[0][1]
        info = {'url': data.get('URL'),
                'lastchangedauthor': data.get('last_changed_author'),
                'revision': data.get('rev').number,
                'commit_time': datetime.fromtimestamp(data.get('last_changed_date')).strftime('%c')}
        return info

    def get_externals(self, recurse=True):
        """
        Gets externals info for the Svn object url/path

        :param recurse: If true, try to retrieve externals recursively in the project folders
        :returns: list of dictionaries containing the external information`: folder, url, peg revision and operative revision

        Example:
            >>> svn = Svn('url/or/path')
            >>> externals = svn.get_externals()
        """
        # Patterns used in parsing externals lines
        pat_url    = r'(?P<url>https?://(?:[a-zA-Z0-9\._-]+)(?:[a-zA-Z0-9/\._-]+))'
        pat_folder = r'(?P<folder>[a-zA-Z0-9/\.-_]+)'
        pat_pegrev = r'(?:@(?P<peg_rev>\d+))'
        pat_oprev  = r'(?:-r\s?(?P<op_rev>\d+))'

        # Different externals formats
        regex_externals = {
            'CaseA': re.compile(r'^\s*{folder}\s+{url}$'.format(folder=pat_folder, url=pat_url)),
            'CaseB': re.compile(r'^\s*{folder}\s+{oprev}\s+{url}$'.format(folder=pat_folder, oprev=pat_oprev, url=pat_url)),
            'CaseC': re.compile(r'^\s*{url}\s+{folder}$'.format(folder=pat_folder, url=pat_url)),
            'CaseD': re.compile(r'^\s*{oprev}?\s*{url}{pegrev}\s*{folder}$'.format(folder=pat_folder, oprev=pat_oprev, pegrev=pat_pegrev, url=pat_url)),
        }

        log.debug('Collecting externals details')
        raw = self.get_property('svn:externals', recurse=recurse)

        svnexternals = []
        for line in raw.split('\n'):
            for case in regex_externals:
                match = re.search(regex_externals[case], line)
                if match:
                    # Remove null values before appending
                    svnexternals.append(dict((k,v) for k, v in match.groupdict().iteritems() if v))
                    break
        if len(svnexternals) > 0:
            log.debug('Found %d externals for "%s"', len(svnexternals), self.url_or_path)
            return svnexternals
        log.info('Externals not found for "%s"', self.url_or_path)
        return []

    def get_jira_project(self, svnprops=settings.SVNPROPS_JIRAKEY):
        """
        Gets JIRA project key from svn properties
        :param svnprops: list of possible properties to get this info from
        :return: string with JIRA key. ie: PRJKEY or empty string
        """
        log.debug('Getting JIRA project key')
        for prop in svnprops:
            try:
                p = self.get_property(prop)
                log.debug('Obtained JIRA key: %s' % p)
                return p
            except Exception, e:
                pass
        log.error('Can\'t get jira project from vcs properties')
        return ''

    def pinned_externals(self):
        """
        Returns a dictionary list with the externals pinned at the current revision
        :return: dictionary list
        """
        externals = self.get_externals()
        for ext in externals:
            log.debug('External: %s' % ext)
            if not 'peg_rev' in ext:
                ext['peg_rev'] = Svn(ext['url']). get_info()['revision']
        return externals