from tests import *

class TestBuild(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.sample_bamboo_build = {
            'bamboo_agentId':'42696748',
            'bamboo_agentWorkingDirectory':'/opt/ds/bamboo-2/xml-data/build-dir',
            'bamboo_artifactory_path':'ML/test',
            'bamboo_artifactory_repo':'sandbox',
            'bamboo_build_working_directory':'/opt/ds/bamboo-2/xml-data/build-dir/ML-MLT3-PC',
            'bamboo_buildKey':'ML-MLT3-PC',
            'bamboo_buildNumber':'328',
            'bamboo_buildPlanName':'Market Links - ML_Template_v3_DO_NOT_USE - Pre-Build Checks',
            'bamboo_buildResultKey':'ML-MLT3-PC-328',
            'bamboo_buildResultsUrl':'http://bamboo.ams.optiver.com/browse/ML-MLT3-PC-328',
            'bamboo_buildTimeStamp':'2014-11-07T22:00:22.642+01:00',
            'bamboo_jira_username':'notjira',
            'bamboo_jira_version': 'notjira',
            'bamboo_loglevel':'INFO',
            'bamboo_planKey':'ML-MLT3',
            'bamboo_planName':'Market Links - ML_Template_v3_DO_NOT_USE',
            'bamboo_planRepository_branchName':'trunk',
            'bamboo_planRepository_name':'ML Project',
            'bamboo_planRepository_previousRevision':'367',
            'bamboo_planRepository_repositoryUrl':'http://svn.ams.optiver.com/svn/test_ams/ML/gw_ing_fix_e-TEST/trunk',
            'bamboo_planRepository_revision':'367',
            'bamboo_planRepository_type':'optiver-svn',
            'bamboo_product_name':'ml_template',
            'bamboo_release_notes_extra_args':'',
            'bamboo_repository_branch_name':'trunk',
            'bamboo_repository_git_branch':'ML_Build_v3',
            'bamboo_repository_git_repositoryUrl':'ssh://git@stash.ams.optiver.com:7999/ml/ml-build-scripts.git',
            'bamboo_repository_git_username':'',
            'bamboo_repository_name':'DS - pyBuild - DEV',
            'bamboo_repository_optiversvn_repositoryUrl':'http://svn.ams.optiver.com/svn/test_ams/ML/gw_ing_fix_e-TEST/trunk',
            'bamboo_repository_previous_revision_number':'367',
            'bamboo_repository_revision_number':'367',
            'bamboo_resultsUrl':'http://bamboo.ams.optiver.com/browse/ML-MLT3-PC-328',
            'bamboo_shortJobKey':'PC',
            'bamboo_shortJobName':'Pre-Build Checks',
            'bamboo_shortPlanKey':'MLT3',
            'bamboo_shortPlanName':'ML_Template_v3_DO_NOT_USE',
            'bamboo_working_directory':'/opt/ds/bamboo-2/xml-data/build-dir/ML-MLT3-PC',

        }
        cls.sample_config = {
            'productName': 'some_product',
            'productVersion': 'some_product-1.2.3_alpha',
            'buildNumber': 123,
            'planName': 'Manual random build',
            'artifactoryRepo': 'sabdbox',
            'artifactoryPath': 'DS/test',
            'vcsURL': 'http://svn.ams.optiver.com/svn/test_ams/ML/gw_ing_fix_e-TEST/branches/pof',
            'vcsRevision': 341,
            'vcsRepoType': 'svn',
            'vcsRepoBranch': 'pof',
        }
        cls.bamboo = BambooClient()

    def set_environment(self, env):
        for k in env:
            os.environ[k] = env[k]


    @mock.patch('pybuild.managers.BambooClient.get_server_version')
    def test__init_bamboo_build(self, mock_get_bamboo_version):

        self.set_environment(self.sample_bamboo_build)
        mock_get_bamboo_version.return_value = "Bamboo 5.7.2"

        log.info('\n\nBamboo, Subversion, Artifactory, JIRA')
        bamboo = BambooClient()
        artifactory = Artifactory()
        jira = JIRAClient()
        vcs = Svn(url_or_path=os.environ['bamboo_planRepository_repositoryUrl'],
                  revision=os.environ['bamboo_planRepository_revision'])
        build = Build(artifactory=artifactory, bamboo=bamboo, vcs=vcs, jira=jira)
        log.info(build)

        log.info('\n\nBamboo, Subversion, no Artifactory')
        bamboo = BambooClient()
        artifactory = None  #Artifactory()
        vcs = Svn(url_or_path=os.environ['bamboo_planRepository_repositoryUrl'],
                  revision=os.environ['bamboo_planRepository_revision'])
        build = Build(artifactory=artifactory, bamboo=bamboo, vcs=vcs)
        log.info(build)

        log.info('\n\nBamboo, no Subversion, no Artifactory')
        # If we do not provide a VCS, have to somehow provide the build version
        os.environ['bamboo_jira_version'] = '7.8.9_jira_version'
        bamboo = BambooClient()
        artifactory = None
        vcs = None
        build = Build(artifactory=artifactory, bamboo=bamboo, vcs=vcs)
        log.info(build)

    @mock.patch('pybuild.build.build.read_func')
    @mock.patch('pybuild.managers.ConfluenceClient.create_page_tree')
    def test_publish_rns(self, mock_create_page_tree, mock_read):
        self.set_environment(self.sample_bamboo_build)
        build = Build()
        self.assertRaises(SystemExit, build.publish_rns,'Testpage/bla/1.2.3', content='<p>Some content</p>')
        #build.confluence = ConfluenceClient(url='http://devwiki-test.ams.optiver.com', usr='hudson', pwd='Hud50N001')
        build.confluence = ConfluenceClient(url='http://idontcare.com', usr='foo', pwd='bar')
        mock_create_page_tree.return_value = True
        self.assertTrue(build.publish_rns('Testpage/bla/foo-1.2.3', content='Some content'))
        mock_create_page_tree.assert_called_with(space_key='RN', parent_title='Testpage',
                                                 page_tree=['bla','foo-1.2.3'], content='Some content')

        mock_read.return_value = 'Content from file'
        self.assertTrue(build.publish_rns('Testpage/bla/foo-1.2.3'))
        mock_create_page_tree.assert_called_with(space_key='RN', parent_title='Testpage',
                                                 page_tree=['bla','foo-1.2.3'], content=mock_read.return_value)
