from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)

from pybuild.conf import settings

from pybuild.tasks.task import Task
from pybuild.managers import Artifactory, BambooClient, ConfluenceClient, JIRAClient, Svn, Ftp, LDAPOptiver
from pybuild.build import Build

import time
import os


class Publish(Task):
    """Publish to Artifactory
    """

    def __init__(self):
        Task.__init__(self)
        self.build = None

    @staticmethod
    def configure_parser(parser):
        """ Parser for generic build publish task
        """
        Task.configure_parser(parser)
        parser.add_argument('-l', '--local-folder', dest='local_folder', help='Local folder containing the artifacts')
        parser.add_argument('-a', '--artifactory-path', dest='artifactory_path', help='Full path to deploy to in Artifactory, ie: ML/bin/product/1.2.3')
        parser.add_argument('-t', '--target-repo', dest='target_repo', help='Artifactory repo to deploy to, ie: optiver-ci')
        parser.add_argument('-p', '--name-pattern', dest='name_pattern', help='Artifact naming schema')
        parser.add_argument('--reports', action='store_true', help='Publish build reports to share')

        parser.set_defaults(target_repo=settings.DEFAULT_CI_REPO,   # Default is optiver-ci
                            artifactory_path=None,             # Default is taken from bamboo_artifactory_path
                            local_folder=settings.DEFAULT_ARTIFACTS_DIR,
                            source_folder=settings.DEFAULT_SRC_DIR,  # Default is 'source'
                            product_version=None,
                            product_name=None,
                            build_name=None,
                            )

    def configure(self):
        Task.configure(self)
        self.build = Build(bamboo=BambooClient(), artifactory=Artifactory())

    def do(self, parser):
        Task.do(self, parser)
        parameters = {'build.type': 'bamboo',
                      'publish.date': time.strftime('%d/%m/%Y'),
                      'publish.time': time.strftime('%H:%M:%S'),
                      'publish.user': self.build.get_build_user(),
                      'product.name': self.build.productName,
                      'product.fullName': self.build.full_version_tag}

        self.build.deploy_folder(self.args.target_repo, self.args.artifactory_path, self.args.local_folder,
                                 parameters=parameters)

        if self.args.reports:
            self.build.publish_to_ftp(settings.DEFAULT_REPORTS_FOLDER)