pybuild.build package
=====================

Submodules
----------

pybuild.build.buildConfig module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pybuild.build.buildConfig

.. autoclass:: BuildConfig
    :members:
    :undoc-members:
    :show-inheritance:

----

pybuild.build.build module
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: pybuild.build.build
    :members: Build
    :undoc-members:
    :show-inheritance:

Module contents
---------------

