JIRA
====

.. toctree::
   :maxdepth: 2

.. automodule:: pybuild.managers
.. autoclass:: JIRAClient
    :members: