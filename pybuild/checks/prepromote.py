"""
Generic Pre-Promote checks
"""
from pybuild.common import initialize_logger, log_header, run_command, url_exists
log = initialize_logger(__name__)

from pybuild.conf import settings

from pybuild.managers import Artifactory, BambooClient, ConfluenceClient, JIRAClient, Svn, Ftp, LDAPOptiver
from pybuild.build import Build

from unittest import TestCase, SkipTest

from prebuild import PreBuildChecks

import re, os


class PrePromoteChecks(TestCase):

    @classmethod
    def setUpClass(cls):
        # Configuration, initialize everything for testing purposes
        cls.build = Build(artifactory=Artifactory(),
                          bamboo=BambooClient(),
                          jira=JIRAClient(),
                          confluence=ConfluenceClient())
        cls.build.init_vcs()

    def test_jira_version_exists(self):
        """Checks if a JIRA version exists for the build version"""

        versions = self.build.jira.get_versions(projectKey=self.build.jira_key)
        version_names = [v['name'] for v in versions] if versions else list()
        self.assertTrue(self.build.get_build_version() in version_names
                          or self.build.full_version_tag in version_names,
                        msg=log_header('!') % 'There is no version %s in JIRA project %s, please, create a version before releasing' % (self.build.full_version_tag, self.build.jira_key))

    def test_release_notes_not_in_confluence(self):
        """Checks that the release notes for this version have not already been published to Confluence"""
        page = self.build.confluence.get_content(space_key=settings.RELEASE_NOTES_SPACE, title=self.build.full_version_tag)
        self.assertEqual({}, page, msg=log_header('!') % 'Release notes for %s have already been published' % self.build.full_version_tag)

