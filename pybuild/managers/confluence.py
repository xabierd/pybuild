from ..common import initialize_logger, RestAPIClient, class_repr
log = initialize_logger(__name__)

from ..conf import settings

import json
import sys

class ConfluenceClient(RestAPIClient):
    """
    Interaction with Confluence API using either OAuth or supplied auth.
    You could use either OAuth or (user,pass) when initializing
    """
    def __init__(self, url=settings.CONFLUENCE_URL, auth=None, usr=None, pwd=None):
        if not auth and not usr:
            auth = self.get_auth('confluence')
        RestAPIClient.__init__(self, url, auth=auth, usr=usr, pwd=pwd)
        self._content = '/rest/api/content/'
        self._content_by_id = '%s{id}?expand=body.view' % self._content
        self._content_by_title = '%s?expand=body.view&spaceKey={key}&title={title}' % self._content
        self._content_with_ancestors = '%s{id}?expand=body.storage,version,ancestors' % self._content

    def __repr__(self):
        return class_repr((vars(self)))


    def get_content(self, page_id=None, space_key=None, title=None):
        """
        Retrieves JSON contents of a Confluence page,
        either by pageId or spaceKey and title
        :param page_id: Page ID
        :param space_key: Space Key
        :param title: Page title
        :return: JSON of the page
        """
        if page_id:
            resp = self.rest_get(self._content_by_id.format(id=page_id))
        elif space_key and title:
            resp = self.rest_get(self._content_by_title.format(key=space_key, title=title))
        else:
            log.error('Not enough parameter supplied to retrieve page')
            sys.exit(-1)

        if resp.status_code in [200, 204]:
            page = resp.json()
            if 'body' in page or page['results']:
                log.info('Successfully retrieved page "%s/%s"' % (space_key, title))
                if page_id:
                    return page
                else:
                    return page['results'][0]
            else:
                log.warning('Page "%s/%s" does not exist' % (space_key, title))
                return {}

    def create_page(self, space_key=None, parent_id=None, title=None, content=''):
        """
        :param space_key: Space Key
        :param parent_id: Parent page ID
        :param title: New page title
        :param content: New page content
        :return: pageId on success or if the page already exists, None on error
        """
        log.info('Creating page "%s" in space "%s"' % (title, space_key))
        payload = {'type': 'page',
                   'space': {
                       'key': space_key},
                   'ancestors': [{
                                 'type': 'page',
                                 'id': parent_id}],
                   'title': title,
                   'body': {'storage': {
                                'value': content,
                                'representation': 'storage'}
                           }
        }

        resp = self.rest_post(self._content, data=json.dumps(payload))
        if resp.status_code in [200, 204]:
            log.info('Successfully created page "%s"' % title)
            log.debug(resp.text)
            return resp.json()['id']
        if resp.status_code == 400:
            if 'already exists' in resp.text:
                log.warning('Page already exists "%s": %s ' %(title, resp.text))
                return self.get_content(space_key=space_key, title=title)['id']
        log.error('Error creating page "%s": %s ' %(title, resp.text))
        return None

    def create_page_tree(self, space_key, parent_title, page_tree, content=''):
        """
        Providing a page list ['one', 'two', 'three'] and a spaceKey/title, it creates
        the empty pages spakeKey/title/one/two/three
        :param space_key: Space in which to create page tree
        :param parent_title: Title of the parent page where to create the tree
        :param page_tree: list of nested pages to be created
        :return: None on failure, page ID of the deepest page on success
        """
        log.debug('Creating page tree "%s" in space "%s"' % (page_tree, space_key))
        parent_content = self.get_content(space_key=space_key, title=parent_title)
        if parent_content:
            parent_id = parent_content['id']
        else:
            log.error('Can\'t retrieve parent, either the space %s or the page %s does not exist' % (space_key, parent_title))
        for page in page_tree:
            if page == page_tree[-1]:
                parent_id = self.create_page(space_key=space_key, parent_id=parent_id,
                                             title=page, content=content)
            else:
                parent_id = self.create_page(space_key=space_key, parent_id=parent_id, title=page)
        return parent_id

    def modify_page(self, space_key, title, content, append=True):
        """
        Modify the contents of a page
        :param space_key: Space Key
        :param title: Page title
        :param content: Content to add or replace
        :param append: Set to false to overwrite the contents of the page
        :return: True/False on success/Failure
        """
        #def get_ancestors(pg):
        #    ancestors=[]
        #    for a in pg['ancestors']:
        #        tp = a['type']
        #        id = a['id']
        #        ancestors.append({'type': tp, 'id': id })
        #    return ancestors


        raise Exception(NotImplemented)
        log.info('Modifying page "%s" in space "%s"' % (title, space_key))
        page_id = self.get_content(space_key=space_key, title=title)['id']
        resp = self.rest_get(self._content_with_ancestors.format(id=page_id))
        if resp.status_code in [200, 204]:
            page = resp.json()
            #ancestors = get_ancestors(page)
            if 'body' in page:
                if append:
                    page['body']['storage']['value'] += content
                else:
                    page['body']['storage']['value'] = content
                page['version']['number'] += 1
                #page['ancestors'] = ancestors
                resp = self.rest_put(self._content + page_id, data=json.dumps(page))

                if resp.status_code in [200, 204]:
                    log.info('Page successfully updated: %s/%s' % (space_key, title))
                    return True
            else:
                log.error('Error retrieving page "%s/%s".' % (space_key, title))
                return False
