from tests import *

from pybuild.conf import settings

class TestCommon(TestCase):

    def test_url_exists(self):
        self.assertTrue(url_exists('http://artifactory.ams.optiver.com/artifactory/simple/bamboo/ML/bin/'))
        self.assertFalse(url_exists('http://artifactory.ams.optiver.com/artifactory/simple/bamboo/ML/bin/DOESNT-EXIST'))

    def test_run_command(self):
        cmd = 'ls -las'
        if settings.WIN32:
            cmd = 'dir'
        rc, out = run_command(cmd)
        self.assertTrue('tests' in out, msg=out)