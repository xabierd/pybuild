Artifactory
===========

.. toctree::
   :maxdepth: 2


.. automodule:: pybuild.managers
.. autoclass:: Artifactory
    :members:



Artifactory Secret Build Info API
---------------------------------

The below information has been taken from `here <https://medium.com/@echohack/artifactorys-secret-build-deploy-api-29437c9d170b>`_
and it's not documented by `JFrog <http://www.jfrog.com/confluence/display/RTF/Artifactory+REST+API>`_

::

    def create_build_info(self, build_name, build_number, dependencies, build_dependencies):
        """
        Returns a build info dictionary which is formatted to correctly deploy
        a new build to artifactory.
        Make a put request with this build info to api/build
        """
        build_info = {'version': '1.0.1',
                      'name': build_name,
                      'number': str(build_number),
                      'type': 'GENERIC',
                      'buildAgent': {'name': 'python', 'version': 'python'},
                      'agent': {'name': 'yourAgentName', 'version': 'yourAgentVersion'},
                      'started': datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S.000-0000'),
                      'durationMillis': 0,
                      'principal': self.build_name,
                      'artifactoryPrincipal': self.username,
                      'url': 'http://yourciserver/job/{}/{}'.format(self.build_name, self.build_number),
                      'licenseControl': {
                          'runChecks': False,
                          'includePublishedArtifacts': False,
                          'autoDiscover': False,
                          'licenseViolationsRecipientsList': '',
                          'scopesList': ''},
                      'buildRetention': {
                          'count': -1,
                          'deleteBuildArtifacts': True,
                          'buildNumbersNotToBeDiscarded': []},
                      'modules': [{
                          'id': '{}:{}'.format(build_name, build_number),
                          'artifacts': dependencies,
                          'dependencies': []}],
                      'buildDependencies': build_dependencies}
        return build_info
