#!/bin/env python27

"""
Interactive script to create a svn release branch with pinned externals

Usage
-----
This script prompts for a repository location (path or url) and a product name (used to identify the release branches).
Based on those, it provides the existing release branches and tags.
It will try ot guess the name of the release branch to create (check :class:`pybuild.managers.Svn`), but the user can override this.
, creates a branch and pins the externals :func:`pin_externals`.
"""

from __future__ import print_function

from pybuild.common import initialize_logger, log_header, get_input
log = initialize_logger(__name__)

from pybuild.conf import settings

from pybuild.managers import Artifactory, BambooClient, ConfluenceClient, JIRAClient, Svn, Ftp, LDAPOptiver
from pybuild.build import Build, AutoVersion

from optiver_version import LongVersion

import sys, os, shutil
import argparse
import tempfile

MAXTAGSTOSHOW = 9
SVNPROPS_JIRAKEY = ["bugtraq:url", "jirakey"]

def warning(*objs):
    print("WARNING: ", *objs, file=sys.stderr)
def error(*objs):
    print("ERROR: ", *objs, file=sys.stderr)


def configure_parser():
    parser = argparse.ArgumentParser('Make release branch')
    parser.add_argument('-s', '--source', help='path to working copy to branch from')
    parser.add_argument('-p', '--product-name', help='Name of the product. Used as branch prefix')
    parser.add_argument('--no-jira', action='store_true', help='Skip JIRA version check')
    parser.add_argument('-j', '--jira', help='JIRA project')
    parser.add_argument('-d', '--dry-run',action='store_true', help='Dry run. Does not make any changes')
    parser.add_argument('--loglevel', help='Log level: (INFO|DEBUG|WARNING). Default is INFO')
    parser.set_defaults(source=settings.MAKERB_DEFAULT_SOURCE,
                        product_name='',
                        loglevel='INFO')
    return parser

def get_latest_tags(url, max=MAXTAGSTOSHOW):
    """
    Gets the latest tags corresponding to the provided URL. It tries to parse the tags through a OptiverVersion
    LongVersion() object. It default to sort by date, if the parsing is unsuccessful.

    :param url: Repo URL, it doesn't have to be a *tags* URL since the Svn object will try to guess the tags URL based on the provided schema (or default to branches/tags/trunk)
    :param max: Maximum number of tags to return
    """
    svn = Svn(url_or_path=url)
    try:
        # Create a tuple with current tag and coerced version. Will sort by version but return the original string
        external_tags = [(LongVersion(it), it) for it in svn.get_tags()]
        return [it[1] for it in sorted(external_tags)[-max:]]
    except Exception, e:
        warning('%s: version format not standard. Showing the last %d tags by date' % (url, max))
        ls = sorted([ (it.get('time'), it.get('name').split('/')[-1]) for it in
               svn.client.ls(svn.tags_url)])[-max:]
        return [i[1] for i in ls]


def pin_external(external):
    """
    For the provided external, offers the user a range of options to pin it:
    * For trunk externals, offers the existing tags. In case the user input is empty, it pins the external
    to the latest revision
    * For *tag* externals, lists the latest tags and allows the user to chose a tag to pin the external to
    :param external: Dictionary containing the external data
    """
    svnexternal = Svn(url_or_path=external['url'])
    external_type = None
    for t in ['/trunk', '/tags', '/branches']:
        if t in external['url']:
            external_type = t
            break
    if external_type:
        external_tags = get_latest_tags(external['url'], MAXTAGSTOSHOW)
        if external_tags:
            while True:
                print('\n"%s" is currently set to: %s' % (external['folder'], external['url']))
                print('Latest tags for %s:\n\t%s' % (external['folder'], '\n\t'.join(external_tags)))
                if external_type == '/tags':
                    current_tag = external['url'].split('/tags/')[-1].split('/')[0]
                    msg = '* Pin to tag (current: %s)' % current_tag
                else:
                    msg = '* Pin to tag [leave empty to pin on revision]'
                extrev = get_input(msg, external_tags[-1])
                if extrev:
                    url_suffix = '/'.join(external['url'].split(external_type)[-1].split('/')[2:])
                    new_tag_url = '{tagsurl}/{tagname}/{suffix}'.format(tagsurl=svnexternal.tags_url, tagname=extrev, suffix=url_suffix)
                    try:
                        testsvn=Svn(url_or_path=new_tag_url)
                        testsvn.get_info()
                        external['url'] = new_tag_url
                        break
                    except Exception:
                        error('Tag name "%s" does not exist in %s (URL: %s)' % (extrev, svnexternal.tags_url, new_tag_url))
                else:
                    info = svnexternal.get_info()
                    external['pegrev'] = info['revision']
                    break
    else: # No trunk/tags/branches
        extrev = ''
        while not extrev:
            rev = str(svnexternal.get_info()['revision'])
            extrev = get_input('\n* External "%s" currently se to (floating):%s\n* Pin to latest change revision (no tags found, latest revision: %s)'%
                               (external['folder'], external['url'], rev), rev)
        external['pegrev'] = extrev
    print('-'*10)
    return external


def main():
    parser = configure_parser()
    args = parser.parse_args()

    log.setLevel(args.loglevel)

    print(log_header() % 'Creating release branch')
    source_path = get_input('\n* Path or URL to prepare a release-branch from', args.source)
    product_name = get_input('\n* Product name', args.product_name)

    svn = Svn(url_or_path=source_path, release_branch_starts_with=product_name)

    try:
        info = svn.get_info()
        source_url = info['url']
    except Exception, e:
        error('Can\'t determine svn url from path %s: %s' % (source_path, e.message))
        sys.exit(-1)

    release_branches = sorted(svn.get_release_branches())
    tags = svn.get_tags()
    print('\nExisting release branches\n\t%s' % '\n\t'.join(release_branches))
    print('\nExisting tags\n\t%s' % '\n\t'.join(tags))
    print('')
    av = AutoVersion(svn.branch, tags, release_branches )
    target_tag = str(av.get_version_release_from_branches(append_branch_name=False))
    if not svn.is_release_branch(svn.branch):
        target_tag = target_tag[:-2] + '.x'
    new_branch = get_input('\n* New release branch name', target_tag)

    # Pin externals
    print('\nProcessing externals...\n')
    externals = svn.get_externals()
    old_externals = svn.export_externals(externals)
    for ex in externals:
        log.debug('Procesig external: %s (%s)' % (ex['folder'], ex['url']))
        if 'oprev' in ex:
            # Pinned to operational revision
            log.debug('Pinned to operational revision: %s' % ex['oprev'])
            continue
        if 'pegrev' in ex:
            # Pinned to peg revision
            log.debug('Pinned to peg revision: %s' % ex['pegrev'])
            continue
        ex = pin_external(ex)

    log.debug('Old externals')
    log.debug(old_externals)
    log.debug('New externals')
    new_externals = svn.export_externals(externals)
    log.debug(new_externals)

    # Check if JIRA version exists
    #TODO: Fix JIRA authentication
    """
    if not args.no_jira:
        jira_project = args.jira if args.jira else svn.get_jira_project()
        jira_project = get_input('\n* JIRA project key', jira_project)
        jira = JIRAClient(projectKey=jira_project)
        jira_version = (target_tag[:-2] + '.0') if target_tag.endswith('.x') else target_tag
        if not jira.version_exists(jira_version):
            error('There is no version in JIRA {prj} corresponding to {ver}\nhttp://jira.ams.optiver.com/browse/{prj}'.format(
                prj=jira_project, ver=jira_version))
            sys.exit(-1)
        else:
            print('Version %s exists in JIRA' % jira_version)
    """

    # Summary
    new_branch_url = '/'.join([svn.branches_url, new_branch])
    print(log_header() % 'The following is about to happen:')
    print('\n1. New branch named "%s" will be created at:\n\t%s\n' % (new_branch, new_branch_url))

    print('2. The following will be changed in externals for this branch:\n')
    new_externals_lines = new_externals.split('\n')
    old_externals_lines = old_externals.split('\n')
    for n in range(0, len(new_externals_lines)):
        if new_externals_lines[n] != old_externals_lines[n]:
            print('\tOLD : %s\n\tNEW > %s\n' % (old_externals_lines[n], new_externals_lines[n]))
        else:
            print('\tSAME: %s\n' % (old_externals_lines[n]))

    # Execute
    if not args.dry_run:
        while True:
            confirm = get_input('\n* Are you sure (yes/no)', '')
            if confirm == 'no':
                error('User aborted...')
                sys.exit(-1)
            if confirm == 'yes':
                break
        print('Creating branch...')
        svn.copy(source_path, new_branch_url, message='pyBuild: Creating release branch for %s' % target_tag)
        tmp_dir = tempfile.mkdtemp(prefix='pyBuild_')
        svn_newbranch=Svn(url_or_path=new_branch_url)
        print('Checking out temporary empty working copy (%s)' % tmp_dir)
        svn_newbranch.checkout(tmp_dir, depth=svn.depth_empty)
        print('Setting new externals...')
        svn_newbranch.set_property('svn:externals', new_externals, tmp_dir)
        print('Commiting changes...')
        svn_newbranch.commit(msg='pyBuild: pinning externals for %s branch' % target_tag)
        shutil.rmtree(tmp_dir)

        # Checkout
        while True:
            confirm = get_input('\n* Do you want to checkout that branch in %s in the current folder (yes/no)' % new_branch, '')
            if confirm == 'no':
                error('User aborted...')
                sys.exit(-1)
            if confirm == 'yes':
                break
        svn2 = Svn(url_or_path=new_branch_url)
        svn2.checkout(os.path.join(os.getcwdu(), new_branch))
    else:
        print('[Dry run, not making any changes]')
    print('Done!!\nYour new branch is: %s' % new_branch_url)


if __name__ == "__main__":
    main()

