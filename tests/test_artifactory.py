from tests import *

from mock import MagicMock as MM

from collections import namedtuple

file_spec = ['_CHUNK_SIZE', '__enter__', '__eq__', '__exit__',
    '__format__', '__ge__', '__gt__', '__hash__', '__iter__', '__le__',
    '__lt__', '__ne__', '__next__', '__repr__', '__str__',
    '_checkClosed', '_checkReadable', '_checkSeekable',
    '_checkWritable', 'buffer', 'close', 'closed', 'detach',
    'encoding', 'errors', 'fileno', 'flush', 'isatty',
    'line_buffering', 'mode', 'name',
    'newlines', 'peek', 'raw', 'read', 'read1', 'readable',
    'readinto', 'readline', 'readlines', 'seek', 'seekable', 'tell',
    'truncate', 'writable', 'write', 'writelines']

class ArtifactoryMain(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.artif = Artifactory()


    def test_get_latest(self):
        cases = [{'repo': 'some-ci-repo', 'path': 'foo/bar', 'module': 'somemod', 'sc': 200, 'cnt': '4.5.6'},
                 {'repo': 'ci', 'path': 'foo/bar', 'module': 'nother', 'sc': 404, 'cnt': 'Not found'}]

        for c in cases:
            return_vale = Response()
            return_vale.status_code = c['sc']
            return_vale._content = c['cnt']
            rget = self.artif.rest_get = MM(return_value=return_vale)

            url = '/api/search/latestVersion?g={path}&a={module}&repos={repo}'.format(
                path=c['path'], module=c['module'], repo=c['repo']
            )
            print self.artif.get_latest_version(c['repo'], c['path'], c['module'], release_repo=True)
            rget.assert_called_with(url=url, headers={'Accept': 'text/plain'})

            print self.artif.get_latest_version(c['repo'], c['path'], c['module'])
            rget.assert_called_with(url=url+'&v=*', headers={'Accept': 'text/plain'})


    def test_get_versions(self):
        print self.artif.get_versions('optiver-ci', 'ds/lib', 'foolib')


    @mock.patch('pybuild.managers.artifactory_fs.ArtifactoryPath.open')
    def test_download_latest(self, mock_artif_open):
        cases = [{'repo': 'ci', 'path': 'foo/bar', 'module': 'somemod',
                  'sc': 200, 'ver': '9.10.11', 'cnt': 'content', 'fn': 'module-9.10.11+build888.zip'},
                 {'repo': 'releases', 'path': 'foo/bar', 'module': 'somemod',
                  'sc': 200, 'ver': '8.9.10', 'cnt': 'content', 'fn': 'module-8.9.10.zip'}]

        def mock_open(mock=None, data=None):
            if mock is None:
                mock = MM(spec=file_spec)
            handle = MM(spec=file_spec)
            handle.write.return_value = None
            if data is None:
                handle.__enter__.return_value = handle
            else:
                handle.__enter__.return_value = data
            return mock

        for c in cases:
            def foo_mock():
                return mock_open(), c['fn']

            m = mock_open()
            mock_artif_open.side_effect = foo_mock
            with mock.patch('__builtin__.open', m, create=True):
                if c['repo'] == 'releases':
                    self.artif.download_latest_release(c['path'], c['module'])
                    mock_artif_open.assert_called_with()
                    m.assert_called_with('%s' % c['fn'], 'wb')

                elif c['repo'] == 'ci':
                    self.artif.download_latest_integration(c['path'], c['module'], c['ver'])
                    mock_artif_open.assert_called_with()
                    m.assert_called_with('%s' % c['fn'], 'wb')

    #def test_set_artifact_properties(self):
    #    props = {'foo': 'bar', 'release-tag': 'http://sdjksdj/tag/123/', 'list': '1,2,3'}
    #    self.artif.set_artifact_properties('optiver-releases', 'ML/test', 'ml_template', '0.1.9', props, recursive=True)
