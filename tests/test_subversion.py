from  tests import *

class TestSvn(TestCase):

    #def test__init_remote_svn(self):
    #    self.fail()

    #def test__init_local_svn(self):
    #    self.fail()

    #def test__url_exists(self):
    #    self.fail()

    #def test__get_project_folder(self):
    #    self.fail()
    #

    def test__get_project_base_url(self):
        valid_urls = [
              'http://svn.ams.optiver.com/svn/test_ams/ML/foobar/trunk',
              'http://svn.ams.optiver.com/svn/test_ams/ML/foobar/tags/0.1.0',
              'http://svn.ams.optiver.com/svn/test_ams/ML/foobar/releases/foobar-0.1.x']
        base_url = 'http://svn.ams.optiver.com/svn/test_ams/ML/foobar'
        invalid_urls = ['http://svn.ams.optiver.com/svn/some/random/path']

        for url in valid_urls:
            vcs = Svn(url_or_path=url, revision=290)
            self.assertEquals(base_url, vcs.base_url, msg='Getting wrong base URL for URL %s' %url)

    def test_get_info(self):
        valid_urls = [
              'http://svn.ams.optiver.com/svn/test_ams/ML/foobar/trunk',
              #'http://svn.ams.optiver.com/svn/test_ams/ML/foobar/tags/0.1.0',
              #'http://svn.ams.optiver.com/svn/test_ams/ML/foobar/releases/foobar-0.1.x'
              ]
        for u in valid_urls:
            vcs = Svn(url_or_path=u)
            info = vcs.get_info()
            externals = vcs.get_externals()
            log.debug('Externals:')
            for e in externals:
                print '\t%s' % e


class TestExternals(TestCase):

    externals = [
        ("""http://svn.ams.optiver.com/svn/main/common_cpp/trunk/libs x_common_libs
         http://svn.ams.optiver.com/svn/main/dev_services/AMS/trunk x_build_infra
         """,
         [dict(folder="x_common_libs", url="http://svn.ams.optiver.com/svn/main/common_cpp/trunk/libs"),
          dict(folder="x_build_infra", url="http://svn.ams.optiver.com/svn/main/dev_services/AMS/trunk")]),
        ("""http://svn.ams.optiver.com/svn/main/marketlink_internal_libs/cpp/market_connection_components/expiry_price_generator/tags/2.3.7/libs x_some_libs
         http://svn.ams.optiver.com/svn/main/marketlink/foo@123 x_foo
         x_bar -r 567 http://svn.ams.optiver.com/svn/main/marketlink/bar
         http://svn.ams.optiver.com/svn/main/marketlink/moo@987654 x_bar/sub
            x_shoo http://svn.ams.optiver.com/svn/main/marketlink/shoo
          x_moo/xo http://svn.ams.optiver.com/svn/main/marketlink/moo/xo
         -r60  http://svn.ams.optiver.com/svn/main/marketlink/double@90 x_double
         """,
         [dict(folder="x_some_libs", url="http://svn.ams.optiver.com/svn/main/marketlink_internal_libs/cpp/market_connection_components/expiry_price_generator/tags/2.3.7/libs"),
          dict(folder="x_foo", url="http://svn.ams.optiver.com/svn/main/marketlink/foo", peg_rev="123"),
          dict(folder="x_bar", url="http://svn.ams.optiver.com/svn/main/marketlink/bar", op_rev="567"),
          dict(folder="x_bar/sub", url="http://svn.ams.optiver.com/svn/main/marketlink/moo", peg_rev="987654"),
          dict(folder="x_shoo", url="http://svn.ams.optiver.com/svn/main/marketlink/shoo"),
          dict(folder="x_moo/xo", url="http://svn.ams.optiver.com/svn/main/marketlink/moo/xo"),
          dict(folder="x_double", url="http://svn.ams.optiver.com/svn/main/marketlink/double", peg_rev="90", op_rev="60")])
    ]

    @mock.patch('pybuild.managers.Svn.get_property')
    def test_parse_externals(self, mock_get_property):
        svn = Svn(url_or_path='http://svn.ams.optiver.com/svn/test_ams/ML/gw_ing_fix_e-TEST/trunk')
        for case, expected in self.externals:
            mock_get_property.return_value=case
            result = svn.get_externals()
            self.assertEquals(expected, result, msg="\n%s\n\n%s" % (expected, result))

