from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)

from pybuild.conf import settings

from pybuild.tasks.task import Task
from pybuild.managers import Artifactory, BambooClient, ConfluenceClient, JIRAClient,Svn, Ftp, LDAPOptiver
from pybuild.build import Build

import os

class BuildtaskML(Task):
    """Main Marketlinks build tasks: CMake, make, ctest, lcov and doxygen
    """
    def __init__(self):
        Task.__init__(self)
        self.build = None

    @staticmethod
    def configure_parser(parser):
        """ Parser for ML build task
        """
        Task.configure_parser(parser)

        parser.add_argument('--build-debug', action='store_true', help='Build debug')
        parser.add_argument('--build-release', action='store_true', help='Build release')
        parser.add_argument('--coverage', action='store_true', help='Coverage check')
        parser.add_argument('--cmake-args', help='CMake additional arguments, ie: (note the space after ") " -DARCH=sbn -DPGO=generate/foo"')
        parser.add_argument('--doxygen', action='store_true', help='Run Doxygen')
        parser.add_argument('--upload', action='store_true', help='Upload doxygen report')
        parser.add_argument('--pin-svn-externals', action='store_true', help='Store svn externals revisions in a file')

        parser.set_defaults(
            build_debug=False,
            build_release=False,
            coverage=False,
            cmake_args='',
            doxygen=False,
            upload=False,
            pin_svn_externals=False
        )

    def configure(self):
        Task.configure(self)
        self.build = Build(bamboo=BambooClient())
        self.build.init_vcs()

    def do(self, parser):
        Task.do(self, parser)

        if self.args.pin_svn_externals:
            self.build.export_pinned_svn_externals(settings.PINNED_EXTERNALS)

        if self.args.build_debug:
            self.build.run_cmake(build_dir=self.build.buildDir,
                                 src_dir=self.build.srcDir,
                                 build_debug=True,
                                 cmake_args=self.args.cmake_args)
            #self.build.run_cmake_build()
            self.build.run_make_install(build_dir=self.build.buildDir)
            self.build.run_generic('ctest --verbose', cwd=self.build.buildDir)
            self.build.collect_artifacts_from_folder(os.path.join(settings.DEFAULT_BUILD_DIR, 'release'))

        if self.args.build_release:
            self.build.run_cmake(build_dir=self.build.buildDir, src_dir='source')
            self.build.run_make_install(build_dir=self.build.buildDir)
            self.build.run_generic('ctest', cwd=self.build.buildDir)

            if not self.build.isLib:
                # We do not consider libraries binaries for the time being
                for binary in self.build.binaryNames:
                    self.build.rename_binary_with_version(os.path.join(self.build.buildDir, 'release', binary))
                self.build.collect_artifacts_from_folder(os.path.join(settings.DEFAULT_BUILD_DIR, 'release'))

        if self.args.coverage:
            self.build.run_cmake(build_debug=True, cmake_args=['-DCOVERAGE=1', '-DCAMAKE_INSTALL_PREFIX:PATH=./'])
            self.build.run_make_install()
            self.build.run_generic('ctest', cwd=self.build.buildDir)
            self.build.run_generic('lcov -c -d %s -o coverage.data' % self.build.buildDir)
            self.build.run_generic('genhtml -o coverage/ coverage.data')
            if self.args.upload:
                remote_folder = '/'.join(['pub', self.build.get_artifactoryProductPath(),
                                              'build{0:04d}'.format(int(self.build.buildNumber)), 'coverage'])
                self.build.publish_to_ftp('coverage', remote_folder=remote_folder)

        if self.args.doxygen:
            self.build.run_generic('doxygen docs/Doxyfile', self.build.srcDir)
            if self.args.upload:
                remote_folder = '/'.join(['pub', self.build.get_artifactoryProductPath(),
                                'build{0:04d}'.format(int(self.build.buildNumber)), 'doxygen'])
                self.build.publish_to_ftp('%s/html' % self.build.srcDir, remote_folder=remote_folder)