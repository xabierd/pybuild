from pybuild.common import initialize_logger, log_header, multiple_replace
log = initialize_logger(__name__)

from pybuild.conf import settings
from pybuild.tasks.task import Task

from pybuild.build import Build
from pybuild.managers import Artifactory, BambooClient, ConfluenceClient

import os, sys
from datetime import datetime


WORKDIR = os.getcwdu()

this_dir, this_filename = os.path.split(__file__)
TEMPLATES_DIR = os.path.join(this_dir, '..', '..',"templates")

GENERATOR = 'python %s/source/x_build_infra/release_notes_generator/release_notes_generator.py' % WORKDIR
GENERATE = "%s -t {template}  -s %s/{source} -o {output} -p {jira_key} -v {version} {release_user} {externals}" % (GENERATOR, WORKDIR)
TMPL_TXT  = os.path.join(TEMPLATES_DIR, 'ml_release_notes_template.txt')
TMPL_HTML = os.path.join(TEMPLATES_DIR, 'confluence_ml_rns_template.txt')

RNs_FOLDER = os.path.join(WORKDIR, settings.DEFAULT_ARTIFACTS_DIR, 'reports')
RN_HTML = os.path.join(WORKDIR, 'RELEASE_NOTES.html')
RN_TXT  = os.path.join(RNs_FOLDER, 'RELEASE_NOTES.txt')


DATE = datetime.now().strftime('%Y-%m-%d %H:%M:%S')


class ReleasenotesML(Task):
    """Marketlinks generate and publish release notes
    """

    def __init__(self):
        Task.__init__(self)

    @staticmethod
    def configure_parser(parser):
        Task.configure_parser(parser)
        parser.description = 'ML Generate release notes'
        parser.add_argument('--user', dest='release_user')
        parser.add_argument('--out', dest='output',help='File to write RNs to')
        parser.add_argument('--version', dest='version')
        parser.add_argument('--extra-args', dest='release_notes_extra_args')
        parser.add_argument('--jira-key', dest='jira_key', help='JIRA project key'),
        parser.add_argument('--source', dest='source')
        parser.add_argument('-e','--externals', dest='externals', nargs='+')
        parser.add_argument('--confluence', action='store_true', help='Publish to Confluence')
        parser.add_argument('--foo', action='store_true')

    def configure(self):
        Task.configure(self)
        self.build = Build(artifactory=Artifactory(),
                           bamboo=BambooClient(),
                           confluence=ConfluenceClient()
                           )
        self.build.init_vcs()

    def do(self, parser):
        self.parser = parser

        def pre_process_template(input_file, output_file, url, date, revision, author):
            """
            Make changes in template file that used to be handled via svn keywords
            :param input_file: Input template
            :param output_file: Output template
            :param url: VCS URL
            :param date: Release date
            :param revision: VCS revision
            :param author: Build user
            """
            try:
                in_file = open(input_file, 'r').read()
                out_file = open(output_file, 'w+')
            except Exception, e:
                log.error('Problem opening template file: %s' % e.message)
                sys.exit(-1)

            replace_dict = {
                '$HeadURL:': 'HeadURL: %s' % url,
                '$Date:': 'Date: %s' % date,
                '$Revision:': 'Revision: %s' % revision,
                '$Author:': 'Author: %s' % author
            }
            new_template = multiple_replace(replace_dict, in_file)
            out_file.write(new_template)

        self.parser.set_defaults(
            release_user = self.build.get_build_user(),
            output_file = '../RELEASE_NOTES.txt',
            version = '-'.join([self.build.productName,self.build.get_build_version()]),
            releas_notes_extra_args = self.build.release_notes_extra_args,
            jira_key = self.build.vcs.get_jira_project(),
            source = 'source',
            externals = [],
            confluence = False,
            artifactory = False
        )

        self.args = self.parser.parse_args()
        log.debug('Arguments: %s' % self.args)
        externals = ['-e %s' % e for e in self.args.externals] if self.args.externals else ''

        TMP_FILE = WORKDIR + '/Template.tmp'


        if not self.args.jira_key:
            log.error('JIRA key not provided. Unable to get it from VCS')
            sys.exit(-1)

        # Generate RNs.txt and place them in artifacts/reports
        log.info('Generating plain text Release Notes in %s' % RN_TXT)
        if not os.path.isdir(RNs_FOLDER): os.mkdir(RNs_FOLDER)

        pre_process_template(TMPL_TXT, TMP_FILE, self.build.vcsURL, DATE, self.build.vcsRevision, self.build.get_build_user())
        generate = GENERATE.format(
            source = self.args.source,
            template = TMP_FILE,
            output = RN_TXT,
            jira_key = self.args.jira_key,
            version = self.args.version,
            release_user = self.args.release_user,
            externals = externals
        )
        self.build.run_generic(generate)


        if self.args.confluence:
            # Generate HTML RNs and publish to Confluence
            log.info('Generating HTML Release Notes')
            self.build.run_generic("rm -rf templates && mkdir templates && rm -rf rns && mkdir rns")
            pre_process_template(TMPL_HTML, TMP_FILE, self.build.vcsURL, DATE, self.build.vcsRevision, self.build.get_build_user())
            generate = GENERATE.format(
                source = self.args.source,
                template = TMP_FILE,
                output = RN_HTML,
                jira_key = self.args.jira_key,
                version = self.args.version,
                release_user = self.args.release_user,
                externals = externals
            )

            log.debug(generate)
            self.build.run_generic(generate, cwd=self.build.srcDir )
            rns = open(RN_HTML).read()
            log.info('Publishing Release Notes to Confluence')
            self.build.publish_rns('/'.join(['Test', self.build.productName, '-'.join([self.build.productName, self.build.get_build_version()])]), rns)

