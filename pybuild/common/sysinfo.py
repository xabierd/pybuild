"""
SysInfo manager .

.. moduleauthor:: Xabier Davila <xabierdavila@optiver.com>

"""
from pybuild.common import initialize_logger, run_command
log = initialize_logger(__name__)

from pybuild.conf import settings

import sys

def get_sysinfo(system):
    info = dict()
    for k in system:
        log.debug('Running: %s' % system[k])
        ret, out = run_command(system[k])
        log.debug('Result: %s' % out)
        if ret is not 0:
            log.error('Error running: %s' % (system[k]))
            info[k] = ''
        info[k] = out

    return info
