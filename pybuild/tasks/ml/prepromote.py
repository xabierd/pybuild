from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)

from pybuild.tasks.prepromote import PrePromote

class PrePromoteML(PrePromote):
    """Marketlinks pre-promotion checks
    """

    def __init__(self):
        PrePromote.__init__(self)

    @staticmethod
    def configure_parser(parser):
        PrePromote.configure_parser(parser)

    def configure(self):
        PrePromote.configure(self)

    def do(self, parser):
        PrePromote.do(self, parser)
