from setuptools import setup, find_packages


setup(
    name='pybuild',
    version=open('VERSION', 'r').read().strip(),
    url='ssh://git@stash.ams.optiver.com:7999/com/pybuild.git',
    license='',
    author='xabierdavila',
    author_email='xabierdavila@optiver.com',
    description='Build scripts framework',
    packages=find_packages('.'),
    package_dir = {'pybuild': 'pybuild'},
    include_package_data=True,
    install_requires=[
        'setuptools',
        'optiverversion>=0.4.2',
        'distribute==0.7.3',
        'nose==1.3.4',
        'certifi==14.05.14',
        'oauthlib==0.7.2',
        'pycrypto==2.6.1',
        'pyparse==1.1.7',
        'requests==2.4.0',
        'requests-oauthlib==0.4.2',
        'simplejson==3.4.1',
        'pathlib==1.0.1',
        'GitPython==1.0.0'
        ],
    entry_points={
        "console_scripts": [
            "pybuild     = pybuild.ep.pb:main",
            "makerb      = pybuild.ep.make_release_branch:main",
            "getartifact = pybuild.ep.getartifact:main"
        ]
    }
)
