"""
pyBuild main settings
"""
import os, sys, re

from optiverversion.base_regex import reLongVersion

# Artifactory defaults
DEFAULT_RELEASES_REPO = 'optiver-releases'
DEFAULT_CI_REPO       = 'optiver-ci'

DEFAULT_BUILD_DIR      = 'build'      #: Main build folder
DEFAULT_SRC_DIR        = 'source'     #: Source folder
DEFAULT_ARTIFACTS_DIR  = 'artifacts'  #: Artifacts for publishing folder
DEFAULT_REPORTS_FOLDER = 'reports'    #: Output folder for reports (doxygen, coverage,...)
#DEFAULT_RNS_FOLDER     = 'rns'        #: Output folder for RNs in txt format

# Windows
GENERATOR_VS12_x64 = 'Visual Studio 12 Win64'
WIN32 = 'win32' in sys.platform

if WIN32:
    HOSTNAME = os.environ.get('COMPUTERNAME')
else:
    HOSTNAME = os.environ.get('HOSTNAME')

HOME = os.path.join(os.path.expanduser('~'))

# SVN
SVN_SCHEMA = ['trunk', 'branches', 'tags', 'releases']
SVNPROPS_JIRAKEY = ["bugtraq:url", "jirakey"]
BRANCH_SIMPLE_REGEX = re.compile(r'^(?:.*/)?(?:(?P<product>[a-zA-Z0-9_-]*)-)*(?P<version>\d+\.\d+(?:\.\d+|x)*)(?:(_|-)?(?P<prerelease>alpha|beta|rc(\.[a-zA-Z0-9_-]*)?))*')
BRANCH_REGEX_STR = r'^{product}(?:-|_)?(?P<version>(?P<major>(?:0|[1-9][0-9]*))\.(?P<minor>(?:0|[1-9][0-9]*))(?:(?:\.|_|-)?(?P<patch>(?:0|[1-9][0-9]*|x|maintenance)))?(?:(?:-|_)?(?P<prerelease>(?:alpha|beta|rc)(?:\.(?:(?:[1-9][0-9]*)*))*(?:\.(?:[a-zA-Z][a-zA-Z0-9\-\_]*))*))?)$'
PINNED_EXTERNALS = 'pinned_externals.properties'


#Git
GIT_RELEASE_BRANCHES = 'release'
GIT_REMOTE_URL_REGEX = re.compile(r'^(?P<preamble>(?:ssh|http))://[a-zA-Z0-9_-]+@(?P<base_url>[a-zA-Z0-9_-]+(?:\.[a-zA-Z0-9_-]+)*)(?::(?P<port>\d+))?/(?:scm/)?(?P<project>[a-zA-Z0-9_-]+)/(?P<repo>[a-zA-Z0-9_-]+).git$')


TAG_COMMIT_MSG = "Tag: {tag_name}"


# Autoversion
# To be added to versions built from non-release branches
AUTOVERSION_PRERELEASE_PREFIX = 'alpha.'


# Confluence
RELEASE_NOTES_SPACE = 'RN'

# Checks
DEFAULT_NOSE_ARGUMENTS = ['', '-s','--verbosity=2', '--with-xunit']
VERSION_VALIDATION_REGEX = reLongVersion

# Preparation task
DEFAULT_PROJECT_PROPERTIES = 'project.properties'

# Publishing
DEFAULT_ARTIFACT_NAMING_PATTERN  = '{product}-{version}+b{build}.{revision}' #{.platform}'

#Tag task
DEFAULT_TAG_LOG_MESSAGE = 'Tag for {longversion}'  # ie: 'Tag for eml_cme_fix-1.2.3-beta.1

# Promotion task
DEFAULT_PROMOTION_COMMENT = 'Released from Bamboo by {user}'
DEFAULT_PROMOTION_STATUS = 'Released'

# Maker Release Branch
MAKERB_DEFAULT_SOURCE = 'source'

CREDS_FOLDER = os.path.join(HOME, '.pybuild')

# DEV environment
if os.environ.get('bamboo_PYBUILD_DEV', None):
    DEVELOPMENT = True

    ARTIFACTORY_URL = 'http://opamdev0021:8081/artifactory'
    JIRA_URL        = 'http://jira.ams.optiver.com'
    CONFLUENCE_URL  = 'http://confluence.ams.optiver.com'
    BAMBOO_URL      = 'http://bamboo.ams.optiver.com'
    STASH_URL       = 'http://stash.ams.optiver.com'
    FTP_SHARE_URL   = 'share.ams.optiver.com'
    LDAP_URL        = 'ldap://ldap1-ams.optiver.com:3268'

    CREDS_FILE    = os.path.join(CREDS_FOLDER, 'pybuild-dev.conf')
    CREDS_KEY_FILE =  os.path.join(CREDS_FOLDER, 'bamboo_buildscripts_oauth_dev.pem')


# PROD environment
else:
    DEVELOPMENT = False

    ARTIFACTORY_URL = 'http://artifactory.ams.optiver.com/artifactory'
    JIRA_URL        = 'http://jira.ams.optiver.com'
    CONFLUENCE_URL  = 'http://confluence.ams.optiver.com'
    BAMBOO_URL      = 'http://bamboo.ams.optiver.com'
    STASH_URL       = 'http://stash.ams.optiver.com'
    FTP_SHARE_URL   = 'share.ams.optiver.com'
    LDAP_URL        = 'ldap://ldap1-ams.optiver.com:3268'

    CREDS_FILE    = os.path.join(CREDS_FOLDER, 'pybuild.conf')
    CREDS_KEY_FILE =  os.path.join(CREDS_FOLDER, 'bamboo_buildscripts_oauth.pem')

# What to display in build's system Info and how to obtain it
SYSTEM_INFO = dict(
    linux=dict(
        kernelversion='facter kernelversion',
        lsbdistid='facter lsbdistid',
        lsbdistrelease='facter lsbdistrelease',
        architecture = 'facter architecture',
        gcc = 'rpm -qf --queryformat="%{NAME} %{VERSION}\n" `which gcc`',
        gcc_path = 'which gcc',
        cmake='cmake --version'
        ),
    windows=dict(
        os='ver'
    )
)

BUILD_INFO_ARTIFACT = 'build_info.json'
