from  tests import *

class TestAutoversion(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.release_branches =  [
                 {'product': 'foo',
                  'buildv'  : [('trunk', 'foo-0.2.0-alpha.trunk'),
                               ('foo-0.1.x', 'foo-0.1.2'),
                               ('feature-FOO-123', 'foo-0.2.0-alpha.feature-FOO-123')],
                  'nextpatch': [('foo-0.1', 'foo-0.1.2')],
                  'tags'    : ['foo-0.1.0', 'foo-0.1.1'],
                  'release_branches': ['foo-0.1.x'],
                  },

                 {'product': 'baa',
                  'buildv'  : [('trunk', 'baa-1.1.0-alpha.trunk'),
                               ('baa-1.0.x', 'baa-1.0.2'),
                               ('feature-FOO-123', 'baa-1.1.0-alpha.feature-FOO-123')],
                  'nextpatch': [('baa-0.1', 'baa-0.1.3'), ('baa-1.0', 'baa-1.0.2')],
                  'tags'    : ['baa-0.1.0', 'baa-0.1.2', 'baa-1.0.1', 'baa-1.1.2', 'baa-2.1.3'],
                  'release_branches': ['baa-0.1.x', 'baa-1.0.x'],
                  },

                 {'product': 'foo',
                  'buildv'  : [('trunk', 'foo-0.2.0-alpha.trunk'),
                               ('foo-0.1.x', 'foo-0.1.2'),
                               ('feature-FOO-123', 'foo-0.2.0-alpha.feature-FOO-123')],
                  'nextpatch': [('foo-0.1', 'foo-0.1.2'), ('foo-3.0', 'foo-3.0.0')],
                  'tags'    : ['foo-0.1.0', 'foo-0.1.1'],
                  'release_branches': ['foo-0.1.x'],
                  },

                 {'product': 'bar',
                  'buildv'  : [('trunk', 'bar-2.1.0-alpha.trunk'),
                               ('bar-0.1.x', 'bar-0.1.2'),
                               ('bar-2.0_maintenance', 'bar-2.0.0'),
                               ('mybar', 'bar-2.1.0-alpha.mybar')],
                  'nextpatch': [('bar-0.1', 'bar-0.1.2'), ('bar-1.0', 'bar-1.0.1')],
                  'tags'    : ['bar-0.1.0', 'bar-0.1.1', 'bar-1.0.0'],
                  'release_branches': ['bar-0.1.x', 'bar-0.2.x', 'bar-2.0_maintenance'],
                  },

                 {'product': 'prerrelease_a',
                  'buildv'  : [('trunk', 'prerrelease_a-2.3.0-alpha.trunk'),
                               ('prerrelease_a-2.2.x', 'prerrelease_a-2.2.0'),
                               ('prerrelease_a-1.0', 'prerrelease_a-1.0.1'),
                               ('some_branch', 'prerrelease_a-2.3.0-alpha.some_branch')],
                  'nextpatch': [('prerrelease_a-2.2', 'prerrelease_a-2.2.0'), ('prerrelease_a-0.1', 'prerrelease_a-0.1.2'),
                                 ('prerrelease_a-1.0', 'prerrelease_a-1.0.1')],
                  'tags'    : ['prerrelease_a-2.2.0_alpha', 'prerrelease_a-0.1.1', 'prerrelease_a-1.0.0'],
                  'release_branches': ['prerrelease_a-2.1.x', 'prerrelease_a-2.2.x', 'prerrelease_a-1.0'],
                  },
                 {'product': 'NoTags',
                  'buildv'  : [('trunk', 'NoTags-0.2.0-alpha.trunk'),
                               ('NoTags-0.1.x', 'NoTags-0.1.0'),
                               ('notags-0.2.x', 'notags-0.2.0'),
                               ],
                  'nextpatch': [('NoTags-0.3.1', 'NoTags-0.3.1'),  # There are no tags, next patch is the current version
                                ('NoTags-0.3.1-alpha', 'NoTags-0.3.1-alpha'),
                                ],
                  'tags'    : [],
                  'release_branches': ['NoTags-0.1.x', 'notags-0.2.x'],
                  }
        ]

        cls.main = [
            ('beer-0.3.0', 'beer-0.2.1',
             'beer', 'trunk', ['beer-0.1.0', 'beer-0.1.1', 'beer-0.2.0']),
            ('wine-0.5.0', 'wine-0.4.0',
             'wine', 'my_feature', ['wine-0.1.0', 'wine-0.1.1-beta', 'wine-0.4.0-alpha']),
            ('gin-0.7.0', 'gin-0.6.10',
             'gin', 'trunk', ['gin-0.6.9', 'gin-0.1.0', 'gin-0.1.1']),
            ('tonic-0.1.0', 'tonic-0.1.0', 'tonic', 'master', [])
        ]

    def test_next_patch_for_version(self):
        for c in self.release_branches:
            print c['tags']
            for version, expected in c['nextpatch']:
                av = AutoVersion(c['product'], 'doesntmatter', c['tags'], c['release_branches'])
                patch = str(av.next_patch_for_version(LongVersion(version, strict=False)))
                self.assertEqual(expected, patch, msg='For version "{ver}", expected "{ex}", got "{real}"'.format(
                                     ver=version, ex=expected, real=patch))


    def test_get_version_release_from_branches(self):
        for c in self.release_branches:
            for branch, expected in c['buildv']:
                av = AutoVersion(c['product'], branch, c['tags'], c['release_branches'])
                ver = str(av.get_version_release_from_branches(append_branch_name=True))
                self.assertEqual(expected, ver, msg='For branch "{branch}", expected "{ex}", got "{real}"'.format(
                                     branch=av.branch, ex=expected, real=ver))


    def test_get_version_release_from_main(self):
        for (expected_minor, expected_patch,
             product, branch, tags) in self.main:
            av_minor = AutoVersion(product, branch, tags, [], type='trunk_minor')
            av_patch = AutoVersion(product, branch, tags, [], type='trunk_patch')
            ver_minor = str(av_minor.get_version(append_branch_name=False))
            ver_patch = str(av_patch.get_version(append_branch_name=False))
            self.assertEqual(expected_minor, ver_minor, msg='For branch "{branch}", expected {ex}, got {real}'.format(
                             branch=av_minor.branch, ex=expected_minor, real=ver_minor))
            self.assertEqual(expected_patch, ver_patch, msg='For branch "{branch}", expected {ex}, got {real}'.format(
                             branch=av_patch.branch, ex=expected_patch, real=ver_patch))
