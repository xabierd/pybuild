#!/bin/env python27

"""
Download artifact from Artifactory
"""

from pybuild.common import initialize_logger
log = initialize_logger(__name__)

from pybuild.conf import settings
from pybuild.managers import Artifactory

import argparse

def configure_parser():

    parser = argparse.ArgumentParser('getartifact')
    parser.add_argument('artifact_path', help='artifact path, ie: ML/bin')
    parser.add_argument('artifact_name', help='artifact name, ie: ml_foo_bar')
    parser.add_argument('-v', '--version', required=False,
                        help="Just for integration artifacts, will get the latest build of the given version")
    parser.add_argument('-u', '--user', required=False, help='Username')
    parser.add_argument('-p', '--password', required=False, help='Password')
    parser.add_argument('-d', '--destination-path', required=False, help='destination path')
    parser.add_argument('-n', '--destination-name', required=False, help='destination name')
    parser.add_argument('-s', '--server-url',  required=False, help='Artifactory server URL. Defaults to %s' % settings.ARTIFACTORY_URL)
    parser.set_defaults(version=None, server_url=settings.ARTIFACTORY_URL,
                        destination_path='.', destination_name=None,
                        user='anonymous', password='')
    return parser

def main():
    #TODO: Get specific release/build versions, choose Artifactory repo
    parser = configure_parser()
    args = parser.parse_args()

    print args

    artifactory = Artifactory(url=args.server_url, usr=args.user, pwd=args.password)

    if args.version:
        log.info('Getting latest build for %s' % args.artifact_name)
        artifactory.download_latest_integration(args.artifact_path,
                                                args.artifact_name,
                                                args.version,
                                                ci_repo=settings.DEFAULT_CI_REPO,
                                                local_path=args.destination_path,
                                                local_file=args.destination_name)

    else:
        log.info('Getting latest release for %s-%s' % (args.artifact_name, args.version))
        artifactory.download_latest_release(args.artifact_path,
                                            args.artifact_name,
                                            releases_repo=settings.DEFAULT_RELEASES_REPO,
                                            local_path=args.destination_path,
                                            local_file=args.destination_name)
        pass

if __name__ == "__main__":
    main()
