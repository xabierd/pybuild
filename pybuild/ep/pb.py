#!/bin/env python27

"""
pyBuild entry point
"""

from pybuild.common import * # initialize_logger, log_header
log = initialize_logger(__name__)

from argparse import ArgumentParser

import sys
import inspect
import importlib
import pkg_resources

# Import all defined tasks
from pybuild.tasks import *

def main():
    # Common parser for all tasks
    mainparser = ArgumentParser('pybuild')
    mainparser.add_argument('--loglevel', choices=['INFO', 'WARNING', 'DEBUG'], default='INFO', help='Log level')
    mainparser.add_argument('--user', help='User name. Will use this credentials whenever is needed')
    mainparser.add_argument('--pass', help='Password')

    subparsers = mainparser.add_subparsers(dest='task', title='tasks')

    # Configure subparsers for every task.
    # For a task to be visible, it has to be imported in tasks/__init__.py
    tasks = inspect.getmembers(sys.modules['pybuild.tasks'], inspect.isclass)
    task_list = dict()
    for t in tasks:
        task_class_name , task_class_object = t
        task_name = str(task_class_name.lower()) # Convert to lowercase
        # dict with lower case task name and proper class name capitalization
        task_list[task_name] = task_class_name
        # Get help from each task class docstring firs line
        first_doc_line = globals()[task_class_name].__doc__.splitlines()[0]
        t_parser = subparsers.add_parser(task_name, help = first_doc_line)
        task_class_object.configure_parser(t_parser)

    args = mainparser.parse_args()

    task_class = getattr(importlib.import_module('pybuild.tasks'), task_list[args.task])

    pybuild_version = pkg_resources.get_distribution('pybuild')
    log.info(log_header() % 'Starting pyBuild %s' % pybuild_version)

    task = task_class()
    task.configure()
    task.do(mainparser)

if __name__ == "__main__":
    main()