"""
Post-build generic tests
"""

from ..common import initialize_logger, log_header, run_command
log = initialize_logger(__name__)

from pybuild.managers import Artifactory, BambooClient, ConfluenceClient, JIRAClient,Svn, Ftp, LDAPOptiver
from pybuild.build import Build

from unittest import TestCase


class PostBuildChecks(TestCase):

    @classmethod
    def setUpClass(cls):
        # Configuration
        cls.build = Build(artifactory=Artifactory(),
                      bamboo=BambooClient(),
                      jira=JIRAClient())
        cls.build.init_vcs()
        log.info('Build: \n%s' % cls.build)
        run_command('rm -rf _temp && mkdir _temp')
        run_command('cd _temp && unzip -o ../%s/%s.zip' % (cls.build.artifactsDir, cls.build.full_version_dev))
        cls.rc_list = []
        for binary in cls.build.get_full_binary_names():
            rc, out = run_command('_temp/%s -v 2>&1' % binary)
            cls.rc_list.append((rc, out))

    def test_binary_buildinfo_runs(self):
        """Confirm that, for every binary, '-v' produces no error"""
        #TODO: check this for every binary produced by the build
        self.assertTrue(all(rc[0] == 0 for rc in self.rc_list))

    def test_build_is_not_unclean(self):
        """Confirm that the build is nor marked as "UNCLEAN" """
        self.assertFalse(any('UNCLEAN' in rc[1] for rc in self.rc_list),
                         msg='This build is marked as "UNCLEAN" so it\'s not suitable for production')
        #self.assertFalse('UNCLEAN' in self.out)


    def test_build_target_is_release(self):
        """Confirm that the build target is RELEASE"""
        self.assertTrue(all('Build target: Release' in rc[1] for rc in self.rc_list))
        #self.assertTrue('Build target: Release' in self.out)

    def test_confirm_interpol_NOT_enabled(self):
        """ Confirm that Interpol is DISABLED"""
        self.assertTrue(all('Interpol enabled: no' in rc[1] for rc in self.rc_list))
        #self.assertTrue('Interpol enabled: no' in self.out)

