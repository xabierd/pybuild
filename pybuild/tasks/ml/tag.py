from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)

from pybuild.tasks.tag import Tag

class TagML(Tag):
    """Marketlinks create tag
    """

    def __init__(self):
        Tag.__init__(self)

    @staticmethod
    def configure_parser(parser):
        Tag.configure_parser(parser)

    def configure(self):
        Tag.configure(self)

    def do(self, parser):
        Tag.do(self, parser)
