"""ML build preparations
"""

from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)

from pybuild.tasks.prepare import Prepare

class PrepareML(Prepare):
    """Marketlinks preparation task
    """

    def __init__(self):
        Prepare.__init__(self)

    @staticmethod
    def configure_parser(parser):
        Prepare.configure_parser(parser)

    def configure(self):
        Prepare.configure(self)

    def do(self, parser):
        Prepare.do(self, parser)
