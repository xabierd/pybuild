"""
Basic REST API client functionality
"""
from common import initialize_logger, get_auth
log = initialize_logger(__name__)

from ..conf import settings

import requests

import logging

# This is a dirty piece of code to disable annoying logging produced by requests and urllib3
if not settings.DEVELOPMENT: # Uncomment when release
    logging.getLogger("oauthlib").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth1").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth1.rfc5849").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth1.rfc5849.endpoints").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth1.rfc5849.endpoints.access_token").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth1.rfc5849.endpoints.request_token").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth1.rfc5849.endpoints.resource").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth1.rfc5849.endpoints.signature_only").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth2").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth2.rfc6749").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth2.rfc6749.endpoints").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth2.rfc6749.endpoints.authorization").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth2.rfc6749.endpoints.base").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth2.rfc6749.endpoints.resource").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth2.rfc6749.endpoints.revocation").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth2.rfc6749.endpoints.token").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth2.rfc6749.grant_types").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth2.rfc6749.grant_types.authorization_code").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth2.rfc6749.grant_types.base").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth2.rfc6749.grant_types.client_credentials").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth2.rfc6749.grant_types.implicit").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth2.rfc6749.grant_types.refresh_token").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth2.rfc6749.grant_types.resource_owner_password_credentials").setLevel(logging.CRITICAL)
    logging.getLogger("oauthlib.oauth2.rfc6749.request_validator").setLevel(logging.CRITICAL)
    logging.getLogger("requests").setLevel(logging.CRITICAL)
    logging.getLogger("requests.packages").setLevel(logging.CRITICAL)
    logging.getLogger("requests.packages.urllib3").setLevel(logging.CRITICAL)
    logging.getLogger("requests.packages.urllib3.connectionpool").setLevel(logging.CRITICAL)
    logging.getLogger("requests.packages.urllib3.poolmanager").setLevel(logging.CRITICAL)
    logging.getLogger("requests.packages.urllib3.util").setLevel(logging.CRITICAL)
    logging.getLogger("requests.packages.urllib3.util.retry").setLevel(logging.CRITICAL)
    logging.getLogger("requests_oauthlib").setLevel(logging.CRITICAL)
    logging.getLogger("requests_oauthlib.oauth1_auth").setLevel(logging.CRITICAL)
    logging.getLogger("requests_oauthlib.oauth1_session").setLevel(logging.CRITICAL)
    logging.getLogger("requests_oauthlib.oauth2_session").setLevel(logging.CRITICAL)
    #log.setLevel(logging.INFO)

# This has been used to generate the code above
#ldic = sorted(logging.Logger.manager.loggerDict)
#for l in ldic:
#    print 'logging.getLogger("%s").setLevel(logging.CRITICAL)' % l

class RestAPIClient:
    """
    Class representing a REST API client.

    It exposes basic functionality:

    - Authentication
    - GET/PUT/POST

    """
    def __init__(self, url, auth=None, usr=None, pwd=None):
        """
        You can authenticate via `auth` or the `usr` and `pwd` strings

        :param url: base URL for your REST server
        :param auth: authentication token
        :param usr: username
        :param pwd: password
        """
        self.server_url = url
        self.user = usr
        self.password = pwd
        self.json_headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
        if auth:
            self.auth = auth
        else:
            self.auth = (self.user, self.password)

    def get_auth(self, app):
        return get_auth(app)

    def rest_get(self, url, params=None, headers=None):
        """
        REST GET request

        :param url: relative url for the request
        :param params: optional parameters to add the request
        :param headers: optional headers to add to the request
        :return: response tuple
        """
        if not headers: headers = self.json_headers
        log.debug("GET request: {}{}".format(self.server_url, url))
        res = requests.get(self.server_url + url, auth=self.auth,  headers=headers)
        return res

    def rest_put(self, url, params=None, headers=None, data=None):
        """
        REST PUT request

        :param url: relative url for the request
        :param params: optional parameters to add the request
        :param headers: optional headers to add to the request
        :param data: data to use in request
        :return: response tuple
        """
        if not headers: headers = self.json_headers
        log.debug("PUT request: {}{}".format(self.server_url, url))
        log.debug("\tParams: %s" % params)
        log.debug("\tHeaders: %s" % headers)
        res = requests.put(self.server_url + url, params=params,
                           data=data, headers=headers, auth=self.auth)
        return res

    def rest_post(self, url, params=None, headers=None, data=None):
        """
        REST POST request

        :param url: relative url for the request
        :param params: optional parameters to add the request
        :param headers: optional headers to add to the request
        :param data: data to use in request
        :return: response tuple
        """

        if not headers: headers = self.json_headers
        log.debug("POST request: {}{}".format(self.server_url, url))
        log.debug("\tParams: %s" % params)
        log.debug("\tHeaders: %s" % headers)
        res = requests.post(self.server_url + url, params=params,
                            headers=headers, auth=self.auth, data=data)
        #return res.text, res.status_code
        return res
