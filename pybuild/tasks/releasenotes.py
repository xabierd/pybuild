"""Generates the release notes
Uses the RNs generator script stored in x_build_infra
"""

from pybuild.common import initialize_logger, log_header
log = initialize_logger(__name__)

from pybuild.tasks.task import Task

from pybuild.managers import Artifactory, BambooClient, ConfluenceClient, JIRAClient, Svn, Ftp, LDAPOptiver
from pybuild.build import Build

import sys

class Releasenotes(Task):
    """Generic release notes generation and publish
    """

    def __init__(self):
        Task.__init__(self)

    @staticmethod
    def configure_parser(parser):
        Task.configure_parser(parser)

    def configure(self):
        Task.configure(self)
        self.build = Build(bamboo=BambooClient(), confluence=ConfluenceClient(), artifactory=Artifactory())
        self.build.init_vcs()

    def do(self, parser):
        log.error(log_header() % 'Not implemented')
        sys.exit(-1)
        # Task.do(self, parser)