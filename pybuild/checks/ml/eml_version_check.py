"""
EML version check checks
"""
from pybuild.common import initialize_logger, log_header, run_command, url_exists
log = initialize_logger(__name__)

from pybuild.conf import settings

from pybuild.managers import Artifactory, BambooClient, ConfluenceClient, JIRAClient,Svn, Ftp, LDAPOptiver
from pybuild.build import Build

from unittest import TestCase


class EMLVersionCheck(TestCase):

    def test_EML_version(self):
        """DUMMY: Check that the EML versions are correct"""
        self.assertTrue(True)