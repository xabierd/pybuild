#!/bin/bash

# Source the other files
. ${PYBUILD}/bash/utils.sh

#Source the Python virtualenv
. ${BAMBOOENV}

# Checks if all variables are initialized.
# Should be AFTER sourcig the virtualenv
set -u

PATH=$PATH:${PYBUILD}
CHECKSDIR=${PYBUILD}/pybuild/checks

prepare_build(){
    log HEADER 'Prepare Build'
    task_prepare_build.py
}

pre_build_checks(){
    log HEADER 'Pre-Build checks'
    nosetests ${CHECKSDIR}/check_PreBuild.py -v --with-xunit
    # Nosetests arguments
    # -v verbose: displays each test description
    # -s do not capture log USE FOR DEBUG
}

post_build_checks(){
    log HEADER 'Post-Build checks'
    nosetests ${CHECKSDIR}/check_PostBuild.py -v --with-xunit
}

pre_promote_checks(){
    log HEADER 'Pre-Promote Checks'
    # something here
}

build_debug(){
    log HEADER 'Build Debug'
    task_build.py --build-debug
}

build_release(){
    log HEADER 'Build Release'
    task_build.py --build-release
}

coverage(){
    log HEADER 'Coverage'
    task_build.py --coverage
}

doxygen(){
    log HEADER 'Doxygen'
    task_build.py --doxygen
}

artifactory_publish(){
    log HEADER 'Publish to Artifactory and FTP share'
    task_publish bamboo --reports
}

promote_build(){
    log HEADER 'Promote Build'
	task_promote_build bamboo
}

create_tag(){
    log HEADER 'Create tag'
    task_vcs_tag bamboo
}